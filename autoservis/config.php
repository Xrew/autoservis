<?php
  error_reporting(E_ALL);

  require 'models/outlet/Outlet.php';
  Outlet::init(include 'outlet-config.php');

  // Vlastni modely
  require 'models/domain/Zakaznik.php';
  require 'models/domain/Objednavka.php';
  require 'models/domain/StavObjednavky.php';
  require 'models/domain/Zamestnanec.php';
  require 'models/domain/Ukon.php';
  require 'models/domain/Spotreba.php';
  require 'models/domain/Material.php';

  $_LC = array(
    "list" => array(
      "login" => "Přihlášení",
      "objednavky" => "Přehled objednávek",
      "zakaznici" => "Přehled zákazníků",
      "material" => "Materiál na skladě",
      "zamestnanci" => "Zaměstnanci",
      "prosperita" => "Prosperita firmy",
      "ukony" => "Úkony"
    ),
    "new" => array(
      "objednavky" => "Nová objednávka",
      "zakaznici" => "Nový zákazník",
      "material" => "Nový materiál",
      "zamestnanci" => "Nový zaměstnanec",
      "ukony" => "Úkony"
    ),
    "show" => array(
      "objednavky" => "Zobrazit objednávku",
      "zakaznici" => "Zobrazit zákazníka",
      "material" => "Zobrazit materiál",
      "zamestnanci" => "Zobrazit zaměstnance",
      "ukony" => "Úkony"
    ),
    "edit" => array(
      "objednavky" => "Upravit objednávku",
      "zakaznici" => "Upravit zákazníka",
      "material" => "Upravit materiál",
      "zamestnanci" => "Upravit zaměstnance",
      "ukony" => "Úkony"
    ),
    "err" => array(
      "notfound" => "Stránka nebyla nalezena"
    )
  );
?>
