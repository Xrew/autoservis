<div id="content">
  <h2>Stránka nenalezena</h2>
  <p>Adresa, na kterou přistupujete, nebyla nalezena. Pokud si myslíte,
  že se jedná o chybu, nahlaste ji prosím správci informačního systému.</p>
</div>
