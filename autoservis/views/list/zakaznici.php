<?php
  $c = new ZakazniciController;
  $columns = $c->getTableColumns();
  $data = $c->getTableData();
  $pagination = $c->getPagination();
?>
<div id="content">
  <h1><?= $title ?></h1>
  <div class="clear"></div>
  
  <a href="index.php?p=zakaznici&a=new" class="add_btn">Nový zákazník</a>
  <table class="list">
    <tr>
      <? foreach ($columns as $col): ?>
      <th><?= $col ?></th>
      <? endforeach; ?>
      <th></th>
    </tr>
    <? foreach ($data as $zak): ?>
    <tr>
      <td><?= $zak->celeJmeno() ?></td>
      <td><?= $zak->adresa() ?></td>
      <td><?= $zak->telefon ?></td>
      <td class="buttons_cell">
        <?= $app->showButton($zak) ?>
        <?= $app->editButton($zak) ?>
        <?= $app->deleteButton($zak) ?>
      </td>
    </tr>
    <? endforeach; ?>
  </table>
  
  <?= $pagination ?>
</div>
