<?php
  $c = new ObjednavkyController;
  $columns = $c->getTableColumns();
  $data = $c->getTableData();
  $stav = $c->getStav();
  $pagination = $c->getPagination();

?>
<div id="content">
  <div class="switcher">
    <a href="<?= $app->link(array("stav" => "vsechny")) ?>"
      <? if ($stav == "vsechny"): ?>class="active"<? endif; ?>>
      Všechny</a>
    <a href="<?= $app->link(array("stav" => "vyrizene")) ?>"
      <? if ($stav == "vyrizene"): ?>class="active"<? endif; ?>>
      Vyřízené</a>
    <a href="<?= $app->link(array("stav" => "nevyrizene")) ?>"
      <? if ($stav == "nevyrizene"): ?>class="active"<? endif; ?>>
      Nevyřízené</a>
  </div>
  <h1><?= $title ?></h1>
  <div class="clear"></div>
  
  <a href="index.php?p=objednavky&a=new" class="add_btn">Nová objednávka</a>
  <table class="list">
    <tr>
      <? foreach ($columns as $col): ?>
      <th><?= $col ?></th>
      <? endforeach; ?>
      <th></th>
    </tr>
    <? foreach ($data as $objednavka): ?>
    <tr>
      <td>#<?= $objednavka->cisloObjednavky() ?></td>
      <td><?= $objednavka->termin_vyrizeni->format("d.m.Y") ?></td>
      <td><?= $objednavka->getZakaznik()->celeJmeno() ;?></td> 
      <td><?= $objednavka->getStavObjednavky()->nazev ?></td>
      <td><?= $objednavka->getCena() ?></td>
      <td class="buttons_cell">
        <?= $app->showButton($objednavka) ?>
        <?= $app->editButton($objednavka) ?>
        <?= $app->deleteButton($objednavka) ?>
      </td>
    </tr>
    <? endforeach; ?>
  </table>
  
  <?= $pagination ?>
</div>
