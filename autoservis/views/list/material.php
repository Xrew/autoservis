<?php
  $c = new MaterialController;
  $columns = $c->getTableColumns();
  $data = $c->getTableData();
  $stav = $c->getStav();
  $pagination = $c->getPagination();
?>
<div id="content">
  <div class="switcher">
    <a href="<?= $app->link(array("stav" => "vsechen")) ?>" 
    <? if ($stav == "vsechen"): ?>class="active"<? endif; ?>>
      Všechen
    </a>
   
    <a href="<?= $app->link(array("stav" => "nedostupny")) ?>" 
    <? if ($stav == "nedostupny"): ?>class="active"<? endif; ?>>
      Nedostupný
    </a>


    <a href="<?= $app->link(array("stav" => "dostupny")) ?>" 
    <? if ($stav == "dostupny"): ?>class="active"<? endif; ?>>
      Dostupný
    </a>

  </div>
  <h1><?= $title ?></h1>
  <div class="clear"></div>
  
  <a href="index.php?p=material&a=new" class="add_btn">Přidat materiál</a>
  <a href="index.php?p=material&a=export" class="add_btn exp_btn">Export</a>
  <a href="#" onclick="$('#importForm').show(200);" class="add_btn imp_btn">Import</a>
  
  <form action="index.php?p=material&a=import" method="POST" enctype="multipart/form-data" id="importForm">
    <input type="file" name="path"  id="imp_btn_form"/>
    <input type="submit" value="Ulož" id="save_btn_form" />   
  </form>

  <table class="list">
    <tr>
      <? foreach ($columns as $col): ?>
      <th><?= $col ?></th>
      <? endforeach; ?>
      <th></th>
    </tr>
    <? foreach ($data as $mat): ?>
    <tr>
      <td><?= $mat->typ ?></td>
      <td><?= $mat->kod ?></td>
      <td><?= $mat->mnozstvi ?></td>
      <td><?= $mat->getCena() ?></td>
      <td class="buttons_cell">
        <?= $app->showButton($mat) ?>
        <?= $app->editButton($mat) ?>
        <?= $app->deleteButton($mat) ?>
      </td>
    </tr>
    <? endforeach; ?>
  </table>
  
  <?= $pagination ?>
</div>
