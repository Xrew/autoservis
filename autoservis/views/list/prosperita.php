<?php 
  $prosperita = new ProsperitaController;
 // $objednavka = new ObjednavkaController;
  //$zamestnanci = new ZamestnanciController;
  $prijmy = $prosperita->getPrijmyToGraph();
?>

<div id="content">
  <h1><?= $title ?></h1>
  <div id="chartcontainer">
    <script type="text/javascript">    
      var myData = new Array(
      <?php
        $data = ""; 
        foreach($prijmy as $mes => $prijem)
        {
          if($data!="")
            $data.=",";
          $data.= "['".$mes."',".$prijem."]";
        } 
        echo $data;
      ?>
      );
      var myChart = new JSChart('chartcontainer', 'line');
      myChart.setDataArray(myData);
      myChart.setAxisNameX('Mesic');
      myChart.setAxisNameY('Zisk');
      myChart.setTitle('');
      myChart.setSize(700, 350);
      myChart.setAxisPaddingBottom(50);
      myChart.setAxisPaddingLeft(60);
      myChart.setFlagColor('#509ACE');
      myChart.setFlagRadius(3);
      myChart.setFlagWidth(2);
      for(i=1;i<=12;i++)
      {
          myChart.setTooltip([i]);
      }
      
      myChart.draw();
    </script>


  </div>
</div>
