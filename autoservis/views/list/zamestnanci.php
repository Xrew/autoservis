<?php
  $c = new ZamestnanciController;
  $columns = $c->getTableColumns();
  $data = $c->getTableData();
  $stav = $c->getStav();
  $pagination = $c->getPagination();
?>
<div id="content">
  <div class="switcher">
    <a href="<?= $app->link(array("stav" => "vsichni")) ?>"
      <? if ($stav == "vsichni"): ?>class="active"<? endif; ?>>
      Všichni</a>
    <a href="<?= $app->link(array("stav" => "volni")) ?>"
      <? if ($stav == "volni"): ?>class="active"<? endif; ?>>
      Volní</a>
    <a href="<?= $app->link(array("stav" => "zamestnani")) ?>"
      <? if ($stav == "zamestnani"): ?>class="active"<? endif; ?>>
      Zaměstnaní</a>
  </div>
  <h1><?= $title ?></h1>
  <div class="clear"></div>
  
  <a href="index.php?p=zamestnanci&a=new" class="add_btn">Nový zaměstnanec</a>
  <table class="list">
    <tr>
      <? foreach ($columns as $col): ?>
      <th><?= $col ?></th>
      <? endforeach; ?>
      <th></th>
    </tr>
    <? foreach ($data as $zam): ?>
    <tr>
      <td><?= $zam->celeJmeno() ?></td>
      <td><?= $zam->getPozice() ?></td>
      <td><?= $zam->specializace ?></td>
      <td><?= $zam->getVytizeni()==ZAMESTNANEC::VYTIZEN?"Zaměstnán":"Volný" ?></td>
      <td class="buttons_cell">
        <?= $app->showButton($zam) ?>
        <?= $app->editButton($zam) ?>
        <?= $app->deleteButton($zam) ?>
      </td>
    </tr>
    <? endforeach; ?>
  </table>
  
  <?= $pagination ?>
</div>
