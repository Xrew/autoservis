<?php
  global $app;
  $c = new UkonyController;
  $columns = $c->getTableColumns();
  $data = $c->getTableData();
  $stav = $c->getStav();
  $pagination = $c->getPagination();
?>
<div id="content">
  <div class="switcher">
    <a href="<?= $app->link(array("stav" => "vsechny")) ?>"
      <? if ($stav == "vsechny"): ?>class="active"<? endif; ?>>
      Všechny</a>
    <a href="<?= $app->link(array("stav" => "vyrizene")) ?>"
      <? if ($stav == "vyrizene"): ?>class="active"<? endif; ?>>
      Vyřízené</a>
    <a href="<?= $app->link(array("stav" => "nevyrizene")) ?>"
      <? if ($stav == "nevyrizene"): ?>class="active"<? endif; ?>>
      Nevyřízené</a>
  </div>
  <h1><?= $title ?></h1>
  <div class="clear"></div>
  
  <table class="list">
    <tr>
      <? foreach ($columns as $col): ?>
      <th><?= $col ?></th>
      <? endforeach; ?>
      <th></th>
    </tr>
    <? foreach ($data as $ukon): ?>
    <tr>
      <td>#<?= $ukon->cisloUkonu()  ?></td>
      <td><?= $ukon->doba ?></td>
      <td><?= $ukon->typ ;?></td> 
      <td>#<?= $ukon->getObjednavka()->cisloObjednavky() ;?></td> 
      <td><?  $zamestnanec = $ukon->getZamestnanec();
              if($zamestnanec != NULL)
               echo $zamestnanec->celeJmeno();
              else
                echo "Nepřiřazeno";
               ;?></td> 
      <td><?= $ukon->getStav() ?></td>
      <td><?= $ukon->getCena() ?></td>
      <td class="buttons_cell">
        <?= $app->showButton($ukon) ?>
        <?= $app->editButton($ukon) ?>
        <?= $app->deleteButton($ukon) ?>
      </td>
    </tr>
    <? endforeach; ?>
  </table>
  
  <?= $pagination ?>
</div>
