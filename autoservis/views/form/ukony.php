<?php
  $c = new UkonyController;
  $ukon = $c->getUkon();
  $zamestnanec = $ukon->getZamestnanec();
  $zamID = ($zamestnanec) ? $zamestnanec->id : -1;
  $material = $ukon->getMaterial();
  $materialSelect = new Material;
  $materialSelect = $materialSelect->getTableData();
  $seznamMechaniku = $c->seznamMechaniku();
?>
<div id="content">
  <form action="index.php?p=ukony&a=zmena<?php if($_GET['a']!='new') echo '&id='.$_GET['id']?>" method="post">
    <? if ($action == "show"): ?>
    <h1>Úkon č.<?= $ukon->cisloUkonu() ?></h1>
    <? else: ?>
    <h1><?= $title ?></h1>
    <? endif ?>
    <fieldset>
      <legend>Úkon</legend>
      <table>
        <tr>
          <td>Přiřazen:</td>
          <td colspan="3">
            <select name="zamestnanec_id" onchange="selectMechanik(this)">
              <option value="">---</option>
              <? foreach ($seznamMechaniku as $z): ?>
              <option value="<?= $z->id ?>" <?= ($z->id == $zamID) ? "selected" : "" ?>><?= $z->celeJmeno() ?></option>
              <? endforeach; ?>
            </select>
          </td>
        </tr>
        <tr>
          <td>Doba:</td>
          <td>
            <input type="hidden" name="id_ukon" id="fm_id_ukon" value="<?= $ukon->id ?> " />
            <input type="text" name="doba" id="fm_doba" value="<?= $ukon->doba; ?>" class="required" />
          </td>
          <td>Typ:</td>
          <td>
            <input type="text" name="typ" id="fm_typ" value="<?= $ukon->typ; ?>" class="required" />
          </td>
        </tr>
        <tr>
          <td>Objednávka:</td>
          <td>
            <input type="text" name="objednavka" id="fm_objednavka" value="<?= $ukon->getObjednavka()->cisloObjednavky() ; ?>" disabled />
          </td>
          <td>Hotovo:</td>
          <td>
            <select name="stav">
              <option value="0" <?= $ukon->stav?"":"SELECTED" ?>>Ne</option>
              <option value="1"<?= $ukon->stav?"SELECTED":""?>>Ano</option>             
            </select>
          </td>
        </tr>
        <tr>
          <td>Cena:</td>
          <td>
            <input type="text" name="cena" id="fm_cena" value="<?= $ukon->getCena(); ?>" disabled />
          </td>
        </tr>
      </table>
    </fieldset>
    
    <fieldset>
      <legend>Materiál</legend>
      <table class="in_fieldset">
        <tr>
          <th>Název</th>
          <th>Množství</th>
          <th></th>
        </tr>
      <? foreach ($material as $mat): ?>
        <tr>
          <td><?= $mat->getMaterialInfo()->typ ?></td>
          <td><?= $mat->mnozstvi ?></td>
          <td><?= $app->deleteButtonSpotreba($mat) ?></td>
        </tr>
      <? endforeach; ?>
      </table>
      
      <? if ($action != "show"): ?>
      <table style="margin-top:20px;">
        <tr>
          <td>
            <select name="material0" style="width:250px;">
              <? foreach ($materialSelect as $st): ?>
              <option value="<?= $st->kod ?>"><?= $st->kod." (".$st->typ.")" ?></option>
              <? endforeach; ?>
            </select>
          </td>
          <td style="padding-left:100px;">Množství:</td>
          <td>
            <input type="text" name="mnozstvi0"  style="width:50px;" value="0" />
          </td>
        </tr>
        <tr>
          <td colspan="2">
            <input type="button" value="Přidat další materiál" onclick="addNextMaterial(this)" />
          </td>
        </tr>
      </table>
      <? endif; ?>
    </fieldset>
   
    
    <a href="index.php?p=ukony" class="back_btn">Zpět</a>
    <? if ($action != "show"): ?>
      <input type="submit" value="Odeslat" />
    <? endif; ?>
  </form>
</div>
