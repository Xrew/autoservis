<?php
  $c = new ZamestnanciController;
  $zam = $c->getZamestnanec();
  $pozice = $zam->pozice;
?>
<div id="content">
  <form action="index.php?p=zamestnanci&a=zmena<?php if($action != 'new') echo '&id='.$_GET['id']?>" method="post">
    <h1><?= $title ?></h1>
    <fieldset>
      <legend>Přihlašovací údaje</legend>
      <table>
        <tr>
          <td>Uživatelské jméno:</td>
          <td>
            <input type="text" name="uziv_jmeno" value="<?= $zam->uziv_jmeno ?>" class="required" />
          </td>
        </tr>
        <tr>
          <td>Heslo:</td>
          <td>
            <input type="password" name="heslo1" class="equal <? if ($action == 'new'): ?> required<? endif; ?>" />
          </td>
        </tr>
        <tr>
          <td>Heslo (podruhé):</td>
          <td>
            <input type="password" name="heslo2" class="equal <? if ($action == 'new'): ?> required<? endif; ?>" />
          </td>
        </tr>
        <? if ($action != 'new'): ?>
        <tr>
          <td colspan="2">
            <small>Pokud nechcete heslo měnit, nechte pole s hesly prázdná.</small>
          </td>
        </tr>
        <? endif; ?>
      </table>
    </fieldset>
    
    <fieldset>
      <legend>Osobní údaje</legend>
      <table>
        <tr>
          <td>Jméno:</td>
          <td>
            <input type="text" name="jmeno" value="<?= $zam->jmeno ?>" class="required" />
          </td>
          <td>Příjmení:</td>
          <td>
            <input type="text" name="prijmeni" value="<?= $zam->prijmeni ?>" class="required" />
          </td>
        </tr>
        <tr>
          <td>Telefon:</td>
          <td colspan="3">
            <input type="text" name="telefon" value="<?= $zam->telefon ?>" />
          </td>
        </tr>
        <tr>
          <td>Pozice:</td>
          <td>
            <select name="pozice">
              <option value="1" <?= $pozice == 1 ? "selected" : ""; ?>>Mechanik</option>
              <option value="2" <?= $pozice == 2 ? "selected" : ""; ?>>Hlavní mechanik</option>
              <option value="3" <?= $pozice == 3 ? "selected" : ""; ?>>Majitel</option>
            </select>
          </td>
          <td>Specializace:</td>
          <td>
            <input type="text" name="specializace" value="<?= $zam->specializace ?>" />
          </td>
        </tr>
        <tr>
          <td colspan="2"></td>
          <td>Hodinová&nbsp;mzda:</td>
          <td>
            <input type="text" name="hodinova_mzda" value="<?= $zam->mzda ?>" class="integer" />
          </td>
        </tr>
      </table>
    </fieldset>
    
    <a href="index.php?p=zamestnanci" class="back_btn">Zpět</a>
    <? if ($action != "show"): ?>
      <input type="submit" value="Odeslat" />
    <? endif; ?>
  </form>
</div>

