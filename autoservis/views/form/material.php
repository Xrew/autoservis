<?php
  $c = new MaterialController;
  $mat = $c->getMaterial();
  
  
?>
<div id="content">
  <form action="index.php?p=material&a=zmena<?php if($_GET['a']!='new') echo '&id='.$_GET['id']?>" method="post">
    <h1>Nový materiál</h1>
    <fieldset>
      <legend>Materiál</legend>
      <table>
        <tr>
         <td>Kód:</td>
          <td>
            <input type="text" name="kod" value="<?= $mat->kod ?>" class="required" />
          </td>
        </tr>
        <tr>
          <td>Popis:</td>
          <td colspan="3">
            <input type="text" name="typ" class="long" value="<?= $mat->typ ?>" class="required" />
          </td>
        </tr>
        <tr>
          <td>Cena:</td>
          <td>
            <input type="text" name="cena" value="<?= $mat->cena ?>" class="required integer" />
          </td>
          <td>Množství:</td>
          <td>
            <input type="text" name="mnozstvi" value="<?= $mat->mnozstvi ?>" class="required integer" />
          </td>
        </tr>
      </table>
    </fieldset>
    
    <a href="index.php?p=material" class="back_btn">Zpět</a>
    <? if ($action != "show"): ?>
      <input type="submit" value="Odeslat" />
    <? endif; ?>
  </form>
</div>
