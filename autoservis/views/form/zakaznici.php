<?php
  $c = new ZakazniciController;
  $zakaznik = $c->getZakaznik();
?>
<div id="content">
  <form action="index.php?p=zakaznici&a=zmena<?php if($_GET['a']!='new') echo '&id='.$_GET['id']?>" method="post">
    <h1><?= $title ?></h1>
    <fieldset>
      <legend>Zákazník</legend>
      <table>
        <tr>
          <td>Jméno:</td>
          <td>
            <input type="text" name="jmeno" value="<?= $zakaznik->jmeno; ?>" class="required" />
          </td>
          <td>Příjmení:</td>
          <td>
            <input type="text" name="prijmeni" value="<?= $zakaznik->prijmeni; ?>" class="required" />
          </td>
        </tr>
        <tr>
          <td>Ulice:</td>
          <td colspan="3">
            <input type="text" name="ulice" value="<?= $zakaznik->ulice; ?>" class="required" />
          </td>
        </tr>
        <tr>
          <td>Město:</td>
          <td>
            <input type="text" name="mesto" value="<?= $zakaznik->mesto; ?>" class="required" />
          </td>
          <td>PSČ:</td>
          <td>
            <input type="text" name="psc" value="<?= $zakaznik->psc; ?>" class="zip" />
          </td>
        </tr>
        <tr>
          <td>Telefon:</td>
          <td>
            <input type="text" name="telefon" value="<?= $zakaznik->telefon; ?>" />
          </td>
          <td>Email:</td>
          <td>
            <input type="text" name="email" value="<?= $zakaznik->email; ?>"  class="email" />
          </td>
        </tr>
      </table>
    </fieldset>
    
    <a href="index.php?p=zakaznici" class="back_btn">Zpět</a>
    <? if ($action != "show"): ?>
      <input type="submit" value="Odeslat" />
    <? endif; ?>
  </form>
</div>
