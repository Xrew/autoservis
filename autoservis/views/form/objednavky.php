<?php
  $c = new ObjednavkyController;
  $objednavka = $c->getObjednavka();
  $zakaznik = $c->getZakaznik();
  $ukony = $objednavka->getUkony();
  $seznamZakazniku = $c->seznamZakazniku();
  $stavyObjednavky = $c->stavyObjednavky();
?>
<div id="content">
  <form action="index.php?p=objednavky&a=zmena<?php if($_GET['a']!='new') echo '&id='.$_GET['id']?>" method="post">
    <? if ($action == "show"): ?>
    <a href="faktura.php?p=objednavky&a=tisk&id=<?=$_GET['id'] ?>" class="print_btn">Tisknout fakturu</a> 
    <h1>Objednávka č.<?= $objednavka->cisloObjednavky() ?></h1>
    <? else: ?>
    <h1><?= $title ?></h1>
    <? endif ?>
    <fieldset>
      <legend>Zákazník</legend>
      <table>
        <? if ($action != "show"): ?>
        <tr>
          <td colspan="4">
            <select name="zakaznik_id" style="width:278px;" onchange="selectCustomer(this)" value="<?= $zakaznik->id; ?>" >
              <? if ($action == "new"): ?>              
                <option value="">Nový zákazník</option>
              <? elseif ($action =="edit"): ?>
                <option value="<?= $zakaznik->id; ?>">Aktualni: <?= $zakaznik->celeJmeno(); ?></option>
              <? endif; ?>
              <? foreach ($seznamZakazniku as $z): ?>
              <option value="<?= $z->id ?>"><?= $z->celeJmeno() . ", " . $z->adresa() ?></option>
              <? endforeach; ?>
            </select>
          </td>
        </tr>
        <? endif; ?>
        <tr>
          <td>Jméno:</td>
          <td>
            <input type="text" name="jmeno" id="fm_jmeno" value="<?= $zakaznik->jmeno; ?>" class="required" />
          </td>
          <td>Příjmení:</td>
          <td>
            <input type="text" name="prijmeni" id="fm_prijmeni" value="<?= $zakaznik->prijmeni; ?>" class="required" />
          </td>
        </tr>
        <tr>
          <td>Ulice:</td>
          <td colspan="3">
            <input type="text" name="ulice" id="fm_ulice" value="<?= $zakaznik->ulice; ?>" />
          </td>
        </tr>
        <tr>
          <td>Město:</td>
          <td>
            <input type="text" name="mesto" id="fm_mesto" value="<?= $zakaznik->mesto; ?>" />
          </td>
          <td>PSČ:</td>
          <td>
            <input type="text" name="psc" id="fm_psc" value="<?= $zakaznik->psc; ?>" class="zip" />
          </td>
        </tr>
        <tr>
          <td>Telefon:</td>
          <td>
            <input type="text" name="telefon" id="fm_telefon" value="<?= $zakaznik->telefon; ?>" />
          </td>
          <td>Email:</td>
          <td>
            <input type="text" name="email" id="fm_email" value="<?= $zakaznik->email; ?>" class="email" />
          </td>
        </tr>
      </table>
    </fieldset>
    
    <fieldset>
      <legend>Úkony</legend>
      <table>
      <? foreach ($ukony as $ukon): ?>
        <tr>
          <td>
            <input type="text" name="ukony[]" class="long" value="<?= $ukon->typ ?>" />
            <a href="index.php?p=ukony&a=edit&id=<?= $ukon->id ?>"><strong>Zobrazit</strong></a>
          </td>
        </tr>
      <? endforeach; ?>
      <? if ($action != "show"): ?>
        <tr>
          <td>
            <input type="text" name="ukony[]" class="long" />
          </td>
        </tr>
        <tr>
          <td>
            <input type="button" value="Přidat další úkon" onclick="addNextTask(this)" />
          </td>
        </tr>
      <? endif; ?>
      </table>
    </fieldset>
    
    <fieldset>
      <legend>Objednávka</legend>
      <table>
        <tr>
          <td>Stav:</td>
          <td>
            <select name="stav">
              <? foreach ($stavyObjednavky as $st): ?>
              <option value="<?= $st->id ?>" <?= $objednavka->stav==$st->id?"SELECTED":"" ?>><?= $st->nazev ?></option>
              <? endforeach; ?>
            </select>
          </td>
        </tr>
        <tr>
          <td>Číslo&nbsp;vozidla:</td>
          <td>
            <input type="text" name="rc" value="<?= $objednavka->rc_vozidla ?>" class="required" />
          </td>
        </tr>
        <tr>
          <td>Datum&nbsp;podání:</td>
          <td>
            <input type="text" name="datum_podani" value="<?= $objednavka->getDatum() ?>" class="required date" />
          </td>
        </tr>
        <tr>
          <td>Termín&nbsp;vyřízení:</td>
          <td>
            <input type="text" name="termin_vyrizeni" value="<?= $objednavka->getTermin() ?>" class="required date"/>
          </td>
        </tr>
        <tr>
          <td>Popis:</td>
          <td>
            <textarea name="popis"> <?= $objednavka->popis ?></textarea>
          </td>
        </tr>
      </table>
    </fieldset>
    
    <a href="index.php?p=objednavky" class="back_btn">Zpět</a>
    <? if ($action != "show"): ?>
      <input type="submit" value="Odeslat" />
    <? endif; ?>
  </form>

</div>
