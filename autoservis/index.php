<?php
  header('Content-Type: text/html; charset=utf-8');
  
  require "config.php";
  require "AppController.php";
  require 'debug/minified/Debug.php';

  //Debug::enable(E_ALL | E_STRICT);

  $outlet = Outlet::getInstance();
  $outlet->createProxies();
  $outlet->query('SET NAMES UTF8');
 
  $app = new AppController();
  $title = $app->getTitle();
  $user = $app->getUser();
  $page = $app->getPage();
  $sections = $app->getMenuSection();
  $view = $app->getView();
  $action = $app->getAction();
  $errors = $app->getErrors();
  $infos = $app->getInfos();
  $postData = $app->getPostDataInJSON();
?>
<!DOCTYPE HTML>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<title><?= $title ?> | Autoservis</title>
  <link rel="shortcut icon" href="favicon.ico" />
	<link rel="stylesheet" href="style/style.css" type="text/css" />
	<link rel="stylesheet" href="style/jquery-ui-1.8.5.custom.css" type="text/css" />
	<link rel="stylesheet" href="style/jquery.validity.css" type="text/css" />
  <script src="js/jquery-1.4.2.min.js"></script>
  <script src="js/jquery-ui-1.8.5.custom.min.js"></script>
  <script src="js/jquery.validity.pack.js"></script>
  <script src="js/interface.js"></script>
  <script type="text/javascript" src="js/jscharts.js"></script>
  <? if ($action == "show"): ?>
  <script>
  $(document).ready(function(){
    $('input, textarea, select').attr('disabled', 'disabled');
    $('.star').css('display', 'none');
  });
  </script>
  <? endif; ?>
</head>
<body>
<div id="header">
  <div id="login">
    <? if (!$user): ?>
      <a href="index.php?p=login">Přihlásit se</a>
    <? else: ?>
      <a href="index.php?p=zamestnanci&a=edit&id=<?= $user->id?> "><?= $user->celeJmeno() ?></a>&nbsp;&nbsp;|&nbsp;
      <a href="index.php?p=logout">Odhlásit se</a>
    <? endif; ?>
  </div>
  <div id="logo">AUTOSERVIS</div>
</div>

<? if (!empty($user)): ?>
<div id="menu">
<?php
  $menu = array(
    "objednavky" => '<img src="style/linedpaper24.png"/>Objednávky',
    "ukony" => '<img src="style/notecheck24.png"/>Úkony',
    "zakaznici" => '<img src="style/users24.png"/>Zákazníci',
    "material" => '<img src="style/boxupload24.png"/>Materiál',
    "zamestnanci" => '<img src="style/spanner24.png"/>Zaměstnanci',
    "prosperita" => '<img src="style/risegraph24.png"/>Prosperita'
  );
  
  // Vypsat polozky menu
  foreach ($menu as $k => $v) {
    if (!in_array($k, $sections))
      continue;
    
    $active = ($page == $k) ? 'class="active"' : ''; 
    echo "<a href=\"index.php?p=$k\" $active>" . $v . "</a>\n";
  }
?>
</div>
<? endif; ?>
<div id="errors">
<? foreach ($errors as $err): ?>
  <div class="ui-state-error ui-corner-all" style="padding: 0 .1em;">
    <p>
      <span class="ui-icon ui-icon-alert" style="float:left; margin:2px 4px -2px 0;"></span>
      <strong>Chyba: </strong><?= $err ?>
    </p>
  </div>
<? endforeach; ?>
<? foreach ($infos as $info): ?>
  <div class="ui-state-highlight ui-corner-all" style="padding: 0 .1em;">
    <p>
      <span class="ui-icon ui-icon-info" style="float:left; margin:2px 4px -2px 0;"></span>
      <?= $info ?>
    </p>
  </div>
<? endforeach; ?>
</div>
<?php
  if (!empty($infos))
    $app->deleteInfos();
?>

<? if (!empty($errors)): ?>
  <script>
  $(document).ready(function(){
    refreshFormData(<?= $postData; ?>);
  });
  </script>
  <? $app->deleteErrors(); ?>
<? endif; ?>

<? include $view; ?>

<div id="footer">
  <a href="mailto:xsmejk07@stud.fit.vutbr.cz">Vojtěch Smejkal</a>,
  <a href="mailto:xsychr03@stud.fit.vutbr.cz">Tomáš Sychra</a>,
  FIT VUT 2010
</div>
</body>
</html>
