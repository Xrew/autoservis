<?php
return array(
  'connection' => array(
    'dsn'      => 'mysql:host=tomassychra.cz;dbname=tomassychracz',
    'username' => 'tomassychracz001',
    'password' => '9l9t58kovo',
    'dialect'  => 'mysql'
  ),

  'classes' =>  array(
    'Zakaznik' => array(
      'table' =>  'zakaznik',
      'props' =>  array(
        'id'       =>    array('id_zakaznika','int',array('pk'=>true,'autoIncrement'=>true)),
        'jmeno'    =>    array('jmeno','varchar'),
        'prijmeni' =>    array('prijmeni','varchar'),
        'ulice'    =>    array('ulice','varchar'),
        'mesto'    =>    array('mesto','varchar'),
        'psc'      =>    array('PSC','int'),
        'telefon'  =>    array('telefon','varchar'),
        'email'    =>    array('email','varchar')
      ),
      'associations'  =>  array(
        array('one-to-many','Objednavka',array('key'=>'zakaznik')),
       )
    ),

    'Objednavka' => array(
      'table' =>  'objednavka',
      'props' =>  array(
        'id'          =>   array('id_objednavky','int',array('pk'=>true,'autoIncrement'=>true)),
        'popis'       =>   array('popis','varchar'),
        'datum_podani'=>   array('datum_podani','date'),
        'stav'        =>   array('stav','int'),
        'termin_vyrizeni'=> array('termin_vyrizeni','date'),
        'rc_vozidla'     => array('rc_vozidla','varchar'),
        'zakaznik'       => array('zakaznik','int')
      ),
      'associations'  =>  array(
        array('many-to-one','StavObjednavky',array('key'=>'stav')),
        array('many-to-one','Zakaznik',array('key'=>'zakaznik')),
        array('one-to-many','Ukon',array('key'=>'objednavka')),
      )
    ),

    'StavObjednavky' => array(
      'table' =>  'stav_objednavky',
      'props' =>  array(
        'id'   =>  array('id_stavu','int',array('pk'=>true,'autoIncrement'=>true)),
        'nazev'=>  array('nazev','varchar')
      ),
      'associations'  =>  array(
        array('one-to-many','Objednavka',array('key'=>'stav')),
      )
    ),

    'Zamestnanec'  => array(
      'table' =>  'zamestnanci',
      'props' =>  array(
        'id'  =>  array('id_zamestnanec','int',array('pk'=>true,'autoIncrement'=>true)),
        'jmeno'    =>    array('jmeno','varchar'),
        'prijmeni' =>    array('prijmeni','varchar'),
        'uziv_jmeno' =>    array('uziv_jmeno','varchar'),
        'heslo' =>    array('heslo','varchar'),
        'telefon'  =>    array('telefon','varchar'),
        'pozice'   =>    array('pozice','inẗ́'),
        'specializace' => array('specializace','varchar'),
        'odpracovano_hodin' => array('odpracovano_hodin','int'),
        'mzda'    =>     array('mzda','float')
      ),
      'associations'  => array(
        array('one-to-many','Ukon',array('key'=>'zamestnanec')),
      )
    ),

    'Ukon'  =>  array(
      'table' => 'ukon',
      'props' =>  array(
        'id'  =>  array('id_ukonu', 'int',array('pk'=>true,'autoIncrement'=>true)),
        'doba' => array('doba','int'),
        'typ' =>  array('typ','varchar'),
        'objednavka' => array('objednavka','int'),
        'stav' => array('stav','int'),
        'zamestnanec'  =>  array('zamestnanec','int')
      ),
      'associations'  =>  array(
        array('many-to-one','Objednavka',array('key'=>'objednavka')),
        array('many-to-one','Zamestnanec',array('key'=>'zamestnanec')),
        array('one-to-many','Ukon',array('key'=>'idUkon'))
      )
    ),

    'Material'  => array(
      'table' => 'material',
      'props' =>  array(
        'id'  =>  array('id_materialu', 'int',array('pk'=>true,'autoIncrement'=>true)),
        'kod' =>  array('kod','varchar'),
        'typ' =>  array('typ','varchar'),
        'mnozstvi'  =>  array('mnozstvi','float'),
        'cena'  =>  array('cena','float'),
      ),
      'associations' => array(
        array('one-to-many','Material',array('key'=>'idMaterial')),
        
      )
    ),

    'Spotreba'  => array(
      'table' =>  'spotreba',
      'props' =>  array(
        'idUkon'     => array('ukon','int',array('pk'=>true)),
        'idMaterial' => array('material','int',array('pk'=>true)),
        'mnozstvi'   => array('mnozstvi','float'),
      ),
      'associations' => array(
        array('many-to-one','Material',array('key'=>'idMaterial')),
        array('many-to-one','Ukon',array('key'=>'idUkon'))
      ),
    ),  
  )
)
 
       
?>
