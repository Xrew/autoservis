<?php
  require_once "controllers/BaseController.php";
  
  /**
   * Aplikacni kontroler ridi funkcnost cele aplikace.
   */
  class AppController {
    private $page;
    private $action;
    private $user;
    
    public function __construct()
    {
      session_save_path(dirname(__FILE__) . '/sessions');
      session_start();
      
      $this->user = NULL;
      $page = "login";
      $action = "list";
      
      // Uzivatel prihlasen
      if (isset($_SESSION["user"])) {
        if (time() - $_SESSION["time"] < 1800) {
          // Resetovat citac casu relace pri kazde akci
          $_SESSION["time"] = time();

          $outlet = Outlet::getInstance();
          $this->user = $outlet->load("Zamestnanec", $_SESSION["user"]);
            
          if (isset($_GET["p"]))
            $page = $_GET["p"];
          if (isset($_GET["a"]))
            $action = $_GET["a"];
          if ($page == "login")
            $this->goToStartPage();
        }
        // Po 30 minutach odhlasit
        else {
          session_destroy();
          $_SESSION = array();
        }
      }
      
      // Prihlaseni
      if ($page == "login")
        $this->login();
        
      // Odhlaseni
      if ($page == "logout")
        $this->logout();
      
      $this->page = $page;
      $this->action = $action;
        
      // Kontrola existence pohledu
      if (!file_exists($this->getView())) {
        $this->page = "notfound";
        $this->action = "err";
      }

      if($this->page!="notfound")
      {
        //kontrola prav - kdo ma kam pristup
        $defaultPristup = array("login","logout");
        $vedouciPristup = array("login","logout","objednavky","zakaznici","material","zamestnanci","prosperita","ukony");
        $hlMechanikPristup = array("login","logout","objednavky","ukony");
        $mechanikPristup = array("login","logout","ukony");

        $found=0;
        $pozice = "default";
        if (isset($this->user))
          switch($this->user->pozice)
          {
            case ZAMESTNANEC::VEDOUCI :
               $pozice = "vedouci";
            break;

            case ZAMESTNANEC::HLAVNI_MECHANIK :
               $pozice = "hlMechanik";
            break;
          
            case ZAMESTNANEC::MECHANIK :
                $pozice = "mechanik";
            break;
          }

        foreach ( ${$pozice."Pristup"} as $pristup)
           if($pristup == $this->page)
              $found=1;

        if(!$found)
        {
          $this->page = "denied";
          $this->action = "err";
        }
      }


      // mazani polozky z databaze
      if($this->action == "delete")
      {
         switch($this->page)
         {
            case "zakaznici": $table = "Zakaznik";
                 break;
            case "objednavky": $table = "Objednavka";
                 break;
            case "material": $table = "Material";
                 break;
            case "zamestnanci": $table = "Zamestnanec";
                 break;
         }
          try {
            if($this->page=="ukony")
            {
              $spotreba = $outlet->select("Spotreba","WHERE {Spotreba.idUkon} = ? ". 
                                          "AND {Spotreba.idMaterial} = ? ",array($_GET['idUkon'] , $_GET['idMaterial']));             
              $outlet->delete("Spotreba",array($spotreba[0]->idUkon,$spotreba[0]->idMaterial));
              $outlet->delete("Ukon", $_GET['id']);
            }
            else
              $outlet->delete("$table", $_GET['id']);
          }
          catch (PDOException $e)
          {
            $this->addError("Položku nelze smazat, neboť je na ni odkazováno z dalších sekcí.");
          }          
         $this->action="list"; 

                  
      }

      // import a export
      if($this->page == "material")
      { 
        require_once "controllers/MaterialController.php";
        $port = new MaterialController;
        if($this->action == "import")
        {
          $port->importData($this);
          $this->addInfo("Data byla úspěšně importována.");
          $this->redirectTo("material");
        }
        else if ($this->action == "export")
          $port->exportData();
      }       

      // vlozeni noveho zaznamu do databaze
      if($this->action == "zmena")
      {
        switch ($this->page) {
          case "zakaznici" : 
            require "controllers/ZakazniciController.php";
            $zmena= new ZakazniciController;
            $zmena->zmenaZakaznik($this);   
            break;
          case "objednavky" :
            require "controllers/ObjednavkyController.php";
            $zmena= new ObjednavkyController;
            $zmena->zmenaObjednavka($this);   
            break;
          case "material":  
            require_once "controllers/MaterialController.php";       
            $zmena= new MaterialController;
            $zmena->zmenaMaterial($this);      
            break;
          case "zamestnanci": 
            require "controllers/ZamestnanciController.php";
            $zmena= new ZamestnanciController;
            $zmena->zmenaZamestnanec($this);   
            break;
          case "ukony": 
            require "controllers/UkonyController.php";
            $zmena= new UkonyController;
            $zmena->zmenaUkon($this);   
            break;
        }
        if($this->action == "list")
          $this->redirectTo($this->page);   
      }
      
      // Nacist controller
      $conClass = ucwords($this->page) . "Controller";
      $conPath = "controllers/$conClass.php";
      if (file_exists($conPath)) {
        require_once $conPath;
      }
      
    }
    
    // prihlaseni uzivatele
    public function login()
    {
      if (!isset($_POST["jmeno"]) || !isset($_POST["heslo"]))
        return;
     
      $name = $_POST["jmeno"];
      $pass = $_POST["heslo"];
      $outlet = Outlet::getInstance();
      $user = $outlet->select("Zamestnanec",
        "WHERE {Zamestnanec.uziv_jmeno} = ? LIMIT 1", array($name));
        
      if (!empty($user) && $this->passwd($pass) == $user[0]->heslo) {
        $_SESSION["time"] = time();
        $_SESSION["user"] = $user[0]->id;
        $this->user = $user[0];
        $this->goToStartPage();
      }
      else {
        $this->addError("Neplatné uživatelské jméno nebo heslo.");
      }
    }
    
    // odhlaseni uzivatele
    public function logout()
    {
      unset($_SESSION["user"]);
      session_destroy();
      $this->user = NULL;
      $this->redirectTo("login");
    }
    
    // prida chybu do vypisu stranky
    public function addError($msg)
    {
      if (!isset($_SESSION["errors"]))
        $_SESSION["errors"] = array();
      
      $_SESSION["errors"][] = $msg;
    }
    
    // ziska seznam chyb
    public function getErrors()
    {
      return isset($_SESSION["errors"]) ? $_SESSION["errors"] : array();
    }
    
    // vycisti seznam chyb
    public function deleteErrors()
    {
      $_SESSION["errors"] = array();
    }
    
    // prida informaci do vypisu stranky
    public function addInfo($msg)
    {
      if (!isset($_SESSION["infos"]))
        $_SESSION["infos"] = array();
      
      $_SESSION["infos"][] = $msg;
    }
    
    // ziska seznam informaci
    public function getInfos()
    {
      return isset($_SESSION["infos"]) ? $_SESSION["infos"] : array();
    }
    
    // vycisti seznam informaci
    public function deleteInfos()
    {
      $_SESSION["infos"] = array();
    }
    
    // vrati soucasnou akci
    public function getAction()
    {
      return isset($_GET["a"]) ? $_GET["a"] : "";
    }
    
    // vrati prihlaseneho uzivatele
    public function getUser()
    {
      return $this->user;
    }
    
    // presmeruje na jinou stranku
    private function redirectTo($page, $action = "list")
    {
      header("Location: index.php?p=$page&a=$action");
      exit();
    }
    
    // presmeruje na uvodni stranku
    private function goToStartPage()
    {
      if($this->user->pozice == ZAMESTNANEC::MECHANIK)
        $this->redirectTo("ukony");
      else  
        $this->redirectTo("objednavky");
    }
    
    // zahashuje retezec
    public function passwd($str)
    {
      return hash("sha256", $str);
    }
    
    // vrati titulek stranky
    public function getTitle()
    {
      global $_LC;
      $action = ($this->action == "zmena") ? "edit" : $this->action;
      return $_LC[$action][$this->page];
    }
    
    // vrati sekci
    public function getPage()
    {
      return $this->page;
    }
    
    // vrati seznam sekci pro horizontalni menu
    public function getMenuSection()
    {
      if ($this->user)
        switch ($this->user->pozice) {
          case Zamestnanec::MECHANIK:
            return array("ukony");
          case Zamestnanec::HLAVNI_MECHANIK:
            return array("objednavky", "ukony");
          case Zamestnanec::VEDOUCI:
            return array("objednavky", "zamestnanci", "zakaznici",
              "material", "prosperita");
        }
      
      return array();
    }
    
    // vrati nazev layout stranky (view)
    public function getView()
    {
      $layout = "";
      
      switch ($this->action) {
        case "new":
        case "edit":
        case "show":
        case "zmena":
          $layout = "form";
        break;
        
        case "err":
          $layout = "err";
        break;
        
        default:
          $layout = "list";
        break;
      }
      
      return "views/" . $layout . "/" . $this->page . ".php";
    }
    
    // vytvori odkaz
    public function link(array $params)
    {
      // Nepamatovat offset
      unset($_GET["o"]);
      
      $params = array_merge($_GET, $params);
      $url = "index.php?";
      
      foreach ($params as $k => $v)
        $url .= "$k=$v&";
      
      return substr($url, 0, strlen($url) - 1);
    }
    
    // tlacitko zobrazit
    public function showButton($obj)
    {
      $link = "index.php?p=" . $this->getPage() . "&a=show&id=" . $obj->id;
      return "<a href='$link' class='show_btn' title='Zobrazit'>Zobrazit</a>";
    }
    
    // tlacitko upravit
    public function editButton($obj)
    {
      $link = "index.php?p=" . $this->getPage() . "&a=edit&id=" . $obj->id;
      return "<a href='$link' class='edit_btn' title='Upravit'>Upravit</a>";
    }
    
    // tlacitko smazat
    public function deleteButton($obj)
    {
      $link = "index.php?p=" . $this->getPage() . "&a=delete&id=" . $obj->id;
      return "<a href='$link' class='delete_btn' title='Odstranit' onclick='return confirmDelete(this)'>Odstranit</a>";
    }

    // tlacitko smazat spotrebu
    public function deleteButtonSpotreba($sp)
    {
      $link = "index.php?p=" . $this->getPage() . "&a=delete&idUkon=" . $sp->idUkon . "&idMaterial=" . $sp->idMaterial;
      return "<a href='$link' class='delete_btn' title='Odstranit' onclick='return confirmDelete(this)'>Odstranit</a>";
    }

    // nastavi akci
    public function setAction($act)
    {
      $this->action=$act;
    }
    
    // vrati POST data ve formatu JSON
    public function getPostDataInJSON()
    {
      return json_encode($_POST);
    }
  }
?>
