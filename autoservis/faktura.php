<?php
  header('Content-Type: text/html; charset=utf-8');
  
  require "config.php";
  require "AppController.php";
  require "controllers/ZakazniciController.php";
  require "controllers/MaterialController.php";
  require "controllers/ObjednavkyController.php";
  require "controllers/UkonyController.php";

  $outlet = Outlet::getInstance();
  $outlet->createProxies();
  $outlet->query('SET NAMES UTF8');
 
  $app = new AppController();
  $user = $app->getUser();

  /**
  * Pripravime si potrebne udaje pro vystaveni faktury
  */

  //zjistime udaje majitel  
  $majitel = $outlet->select("Zamestnanec","WHERE {Zamestnanec.pozice} = ? ",array(ZAMESTNANEC::VEDOUCI));
  $majitel = $majitel[0];  

  //nadefinujeme potrene kontrolery a zjistime objednavku
  $objC = new ObjednavkyController;
  if($_GET['id']!=NULL)
    $objednavka = $objC->getObjednavkaById($_GET['id']);
  else
    $app->addError("ID objedavky nebylo predano spravne. Kontaktujte prosim admina");
  
  //zjistime udaje o zakaznikovi
  $zakaznik = $objednavka->getZakaznik();
  
  //zjistime ukony na objednavce
  $ukony = $objednavka->getUkony();
    
  
?>
<!DOCTYPE HTML>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<title>Faktura | Autoservis</title>
  <link rel="shortcut icon" href="favicon.ico" />
  <link rel="stylesheet" href="style/faktura.css" type="text/css" />

</head>
<body>
  <div class="fak_form">
     <H1>Faktura</H1>

    <table class="fak_majitel">
      <th class="fak_nadpis" colspan="2" >Údaje dodavatele</th>    
        <tr>
          <td class="atribut">Jméno: </td><td>Tomáš</td> 
        </tr>
        <tr>
          <td class="atribut">Příjmení:</td><td> Sychra</td> 
        </tr>
        <tr>
          <td class="atribut">Adresa:</td><td> K.Čapka 177, Lanškroun 56301</td> 
        </tr>
        <tr>
          <td class="atribut">Číslo účtu : </td><td>smyšlené</td> 
        </tr>
        <tr>
          <td class="atribut">ICO: </td><td>smyšlené</td> 
        </tr>
        <tr>
          <td class="atribut">DIC : </td><td>smyšlené</td> 
        </tr>
    </table>

    <table class="fak_zakaznik" >
      <th class="fak_nadpis" colspan="2">Údaje zákazníka</th>    
        <tr>
          <td class="atribut">Jméno: </td><td><?= $zakaznik->jmeno ?></td> 
        </tr>
        <tr>
          <td class="atribut">Příjmení: </td><td><?= $zakaznik->prijmeni ?></td> 
        </tr>
        <tr>
          <td class="atribut">Adresa: </td><td><?= $zakaznik->adresa() ?></td> 
        </tr>
        <tr>
          <td class="atribut">Telefon : </td><td><?= $zakaznik->telefon ?></td> 
        </tr>
        <tr>
          <td class="atribut">e-mail: </td><td><?= $zakaznik->email ?></td> 
        </tr>
    </table>

    <table class="fak_objednavka">
      <th class="fak_nadpis" colspan="2">Informace o objednávce</th>    
        <tr>
          <td class="atribut">Číslo objednávky:</td><td> <?= $objednavka->cisloObjednavky() ?></td> 
        </tr>
        <tr>
          <td class="atribut">Datum podani:</td><td> <?= $objednavka->getDatum() ?></td> 
        </tr>
        <tr>
          <td class="atribut">Datum vyrizeni:</td><td> <?= $objednavka->getTermin() ?></td> 
        </tr>
        <tr>
          <td class="atribut">RC vozidla:</td><td> <?= $objednavka->rc_vozidla ?></td> 
        </tr>
        <tr>
          <td class="atribut">Popis:</td><td> <?= $objednavka->popis ?></td> 
        </tr>
    </table>

    <hr />
    <table class="fak_ukony"  >
      <p class="fak_nadpisUk" style="text-align:center;">Úkony</p> 

      <th style="text-align:left;">Typ</th>
      <th>Cena</th>  
      <? foreach ($ukony as $ukon): ?>
        <tr>
          <td class="fak_ukTyp" ><?= $ukon->typ ?></td>
          <td class="fak_ukCena"><?= $ukon->getCena()." CZK" ?></td>         
        </tr>  
      <? endforeach; ?>  
      <tr>
        <td><b>Celková cena:</b></td>
        <td class="fak_ukCena"><b><?= $objednavka->getCena()." CZK"?></b></td>
      </tr>  

    </table>
  </div>




</body>
</html>
