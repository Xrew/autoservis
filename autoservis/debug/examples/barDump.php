<!DOCTYPE html><link rel="stylesheet" href="style.css">

<h1>Nette\Debug bar test</h1>

<?php
require_once '../minified/Debug.php';

Debug::enable();

$arr = array(10, 20.2, TRUE, NULL, 'hello', (object) NULL, array());


Debug::barDump(get_defined_vars());

Debug::barDump($arr, 'The Array');

Debug::barDump('<a href="#">test</a>', 'String');
