<?php

class ObjednavkyController extends BaseController {
  private $stav;
  
  public function __construct()
  {
    parent::__construct();
    
    $this->stav = isset($_GET["stav"]) ? $_GET["stav"] : "vsechny";
  }
  
  
  public function show()
  {
    return array(
      
    );
  }
  
  public static function submit()
  {
    
  }
  
  /**
   * - nacte konkretni objednavku z databaze podle id z GET
   * - vyuziva se napr. u formularu
  */
  public function getObjednavka()
  {
    global $outlet;
    
    if (isset($_GET["id"])) {
      $obj = $outlet->load("Objednavka", $_GET["id"]);
      return $obj;
    }
    
    return new Objednavka;
  }
  
  //ziska zakaznika k dane objednavce
  public function getZakaznik()
  {
    global $outlet;
    
    if (isset($_GET["id"])) {
      $obj = $outlet->load("Objednavka", $_GET["id"]);
      if ($obj)
        return $obj->getZakaznik();
    }
    return new Zakaznik;
  }
  
  /**
    * - vraci seznam zakazniku
    * - vyuziva se ve formularich (vyber z nabidky)
   */
  public function seznamZakazniku()
  {
    global $outlet;
    return $outlet->select("Zakaznik");
  }
  
  /**
   * - vraci seznam stavu objednavky (moznosti)
   * - vyuziva se ve formularich (vyber z nabidky)
  */
  public function stavyObjednavky()
  {
    global $outlet;
    return $outlet->select("StavObjednavky");
  }
  
  
  public function getTableColumns()
  {
    return array("Číslo", "Termín", "Zákazník", "Stav", "Cena");
  }
  
  //zuzuje vyber dat z tabulky dle nastaveni (vyrizene/nevyrizene/nedostupny)
  private function getQuery()
  {
    global $outlet;
    $q = $outlet->from("Objednavka");
    
    if ($this->stav == "vyrizene")
      $q = $q->where("{Objednavka.stav} = ?", array(Objednavka::VYRIZENA));
    elseif ($this->stav == "nevyrizene")
      $q = $q->where("{Objednavka.stav} != ?", array(Objednavka::VYRIZENA));
      
    return $q;
  }
  
  //zjistuje pocet radku na stranku kvuli strankovani
  public function dataCount()
  {
    return sizeof($this->getQuery()->find());
  }
  
  public function getTableData()
  {
    return $this->getQuery()
      ->limit($this->limit)
      ->offset($this->offset)
      ->find();
  }

  // vraci stav objednavky  
  public function getStav()
  {
    return $this->stav;
  }

  //metoda pro vkladani a zmenu informaci o objednavce
  public function zmenaObjednavka($app)
  {
    global $outlet;
    $objednavka = new Objednavka;
   
    if(isset($_GET['id']))
      $objednavka=$outlet->load("Objednavka",$_GET['id']);
    
    $objednavka->popis = $_POST['popis'];
    $objednavkal->datum_podani = new DateTime($_POST['datum_podani']);
    $objednavka->termin_vyrizeni = new DateTime($_POST['termin_vyrizeni']);
    $objednavka->rc_vozidla =$_POST['rc'];
    $objednavka->stav = $_POST['stav'];

    //najdeme zakaznika    
    if($_POST['zakaznik_id'] == "")
    {
      $zakaznik = new Zakaznik;
      $zakaznik->jmeno = $_POST['jmeno'];
      $zakaznik->prijmeni = $_POST['prijmeni'];
      $zakaznik->ulice = $_POST['ulice'];
      $zakaznik->mesto = $_POST['mesto'];
      $zakaznik->psc = $_POST['psc'];
      $zakaznik->telefon = $_POST['telefon'];
      $zakaznik->email = $_POST['email'];
      $outlet->save($zakaznik);
    }

    else
      $zakaznik = $outlet->load("Zakaznik",$_POST['zakaznik_id']);

    $objednavka->setZakaznik($zakaznik);
    $outlet->save($objednavka);

    //pridani ukonu k objednavce - nikomu neprirazeny
    $ukonyArray = $_POST['ukony'];

    foreach($ukonyArray as  $ukonTyp)
    {
       if(!$objednavka->existUkon($ukonTyp) && $ukonTyp != "")
       {
         $ukon = new Ukon;
         $ukon->newUkon($ukonTyp);
         $ukon->setObjednavkaToUkon($objednavka);
         $ukon->setZamestnanec($zamestnanec != NULL?$zamestnanec:new Zamestnanec);
         $outlet->save($ukon);
       }
    }
    
   $outlet->save($objednavka);

    //zde bude mozno rozhodovat v pripade chyby co se stane (list/form)
    $app->setAction("list"); 
    return 0;
  }

  //najde objednavku pomoci ID
  public function getObjednavkaByID($id)
  {
    global $outlet;
    return $outlet->load("Objednavka",$id);
  }
  
}

?>
