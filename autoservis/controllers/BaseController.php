<?php

class BaseController {
  protected $offset;
  protected $limit;
  
  public function __construct()
  {
    $this->offset = isset($_GET["o"]) ? intval($_GET["o"]) : 0;
    $this->limit = 15;
  }
  
  public function getPagination()
  {
    $ret = "<div id='pagination'>\n";
    
    global $app;
    $count = $this->dataCount();
    $prev = $this->offset - $this->limit;
    $next = $this->offset + $this->limit;
    
    if ($prev >= 0) {
      $link = $app->link(array("o" => $prev));
      $ret .= "<a href='$link' class='arrow'>&lt;</a>\n";
    }
    
    for ($i = 0; $i < $count; $i += $this->limit) {
      $num = $i / $this->limit + 1;
      $link = $app->link(array("o" => $i));
      $active = ($this->offset == $i) ? "class='active'" : "";
      $ret .= "<a href='$link' $active>$num</a>\n";
    }
    
    if ($next < $count) {
      $link = $app->link(array("o" => $next));
      $ret .= "<a href='$link' class='arrow'>&gt;</a>\n";
    }
    
    $ret .= "</div>\n";
    return $ret;
  }
}

?>
