<?php
/*
* Trida pro export MySQL tabulky do XML
*/

class Xml {
    private $file;
    private $path;

    public function __construct($path)
    {
       $this->path=$path;
    }   
      
    //metoda pro export dat z tabulky material do XML
    public function exportData()
    {
       global $outlet;
       //nacteme pole materialu z databaze
       $material=new Material;
       $material=$outlet->from("Material")->find();
       $exportXML='<?xml version="1.0" encoding="UTF-8"?>';
       $exportXML.="\n<xml>";

       foreach($material as $materialRow)
       {
        $exportXML=$exportXML.
            "\t<column>\n".
                "\t\t<typ>".$materialRow->typ."</typ>\n".
                "\t\t<kod>".$materialRow->kod."</kod>\n".
                "\t\t<mnozstvi>".$materialRow->mnozstvi ."</mnozstvi>\n".
                "\t\t<cena>". $materialRow->cena ."</cena>\n".
             "\t</column>\n";
       } 
       $exportXML=$exportXML."</xml>";   
      
       header("Content-Description: File Transfer"); 
       header("Content-Type: application/force-download"); 
       header("Content-Disposition: attachment; filename=\"$this->path\""); 
       echo $exportXML; 
       exit(0);
    }

    //metoda pro import dat do databaze - vklada jen ty co v databazi nejsou
    public function importData($app)
    {
      global $outlet;
      //pro nacitani dat ze souboru
      $dom=new DOMDocument();
      $dom->preserveWhiteSpace = false;
      $dom->load($this->path);
      $domRoot=$dom->documentElement;     
      $column=$domRoot->firstChild;      
    
      while($column != NULL)
      {
         $data=$column->firstChild;
         $material=new Material;

         while($data!=NULL)
         {
            if($data->tagName=="typ")
            {
              $material->typ=$data->textContent;
            }
            else if($data->tagName=="kod")
            {
              $material->kod=$data->textContent;
            }
            else if($data->tagName=="mnozstvi")
            {
              $material->mnozstvi=$data->textContent;
            }
            else if($data->tagName=="cena")
            {
              $material->cena=$data->textContent;
            }
            else
              $app->addError("Chyba při importu dat.");
            
            $data=$data->nextSibling;
          }
         if(($sth = $outlet->select("Material","WHERE {Material.kod} = ?", array($material->kod)))== NULL)
            $outlet->save($material);
    
        $column=$column->nextSibling;
      }
      
    }
  }
?>
