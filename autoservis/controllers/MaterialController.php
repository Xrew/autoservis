<?php

class MaterialController extends BaseController {
  private $stav;
  public function __construct()
  {
    parent::__construct();
    $this->stav = isset($_GET["stav"]) ? $_GET["stav"] : "vsechen";
  }
  
  
  public function show()
  {
    return array(
      
    );
  }
  
  //nacte material z databaz podle ID z GET (vyuziti napr u upravy materialu)
  public function getMaterial()
  {
    global $outlet;
    
    if (isset($_GET["id"])) {
      $mat = $outlet->load("Material", $_GET["id"]);
      return $mat;
    }
    
    return new Material;
  }

  public function getTableColumns()
  {
    return array("Typ","Kod. označení", "Počet ks", "Cena");
  }
  
  //zuzuje vyber dat z tabulky dle nastaveni (vsechen/dostupny/nedostupny)
  private function getQuery()
  {
    global $outlet;
    $q = $outlet->from("Material");
    
    if ($this->stav == "nedostupny")
      $q = $q->where("{Material.mnozstvi} = ?", array(Material::NEDOSTUPNY));
    elseif ($this->stav == "dostupny")
      $q = $q->where("{Material.mnozstvi} != ?", array(Material::NEDOSTUPNY));
      
    return $q;
  }

  //pocita radky na strance ->kvuli strankovani
  public function dataCount()
  {
     return sizeof($this->getQuery()->find());
  }
  
  //nacita data z databaze
  public function getTableData()
  {
    global $outlet;
      return $this->getQuery()
      ->limit($this->limit)
      ->offset($this->offset)
      ->find();
  }

   //vraci stav stranky (vsechen/dostupny/nedostupny)
  public function getStav()
  {
    return $this->stav;
  }

  //zajistuje obsluhu metod pro import dat z XML
  public function importData($app) 
  {
    global $outlet;
    require "Xml.php";
    $import = new Xml($_FILES['path']['tmp_name']);
    $import->importData($app);
  }

  //zajistuje obsluhu metod pro export dat z tabulky material do XML
  public function exportData() 
  {
    global $outlet;
    require "Xml.php";
    $export = new Xml("export.xml");
    $export->exportData();
  }

  //metoda pro vkladani a zmenu informaci o materialu
  public function zmenaMaterial($app)
  {
    global $outlet;
    $material = new Material;
    $id = isset($_GET["id"]) ? $_GET["id"] : 0;
   
    if(isset($_GET['id']))
      $material = $outlet->load("Material", $id);
    
    $material->kod = $_POST['kod'];
    $material->typ = $_POST['typ'];
    $material->mnozstvi = $_POST['mnozstvi'];
    $material->cena = $_POST['cena'];
   
    // kontrola proti duplicitnimu vkladani
    $dupl = $outlet->select("Material", "WHERE {Material.kod} = ?", array($material->kod));
    if ($dupl and ($id == 0 or $id != $dupl[0]->id))
      $app->addError("Material s tímto kódovým označením již existuje.");
    else
      $outlet->save($material);

    $errs = $app->getErrors();
    if (!empty($errs))
      $app->setAction("edit");
    else
      $app->setAction("list");
  }
}

?>
