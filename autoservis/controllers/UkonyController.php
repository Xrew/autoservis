<?php

class UkonyController extends BaseController {
  private $stav;
  
  public function __construct()
  {
    parent::__construct();
    
    $this->stav = isset($_GET["stav"]) ? $_GET["stav"] : "vsechny";
  }
  
  public function show()
  {
    return array(
      
    );
  }
  
  public static function submit()
  {
    
  }
  
  /**
  * - nacte ukon z databaze podle id z GET
  * - vyuziva se napr. v formulari Ukonu
  */
  public function getUkon()
  {
   global $outlet;
    
    if (isset($_GET["id"])) {
      $uk = $outlet->load("Ukon", $_GET["id"]);
      return $uk;
    }

    return new Ukon;
  }
  
  public function getTableColumns()
  {
    return array("Číslo", "Doba", "Typ", "Objednávka", "Zaměstnanec", "Hotovo", "Cena");
  }
  
  //zuzuje vyber, ktery bude vypsan v tabulce v zalozce ukony
  private function getQuery()
  {
    global $outlet;
    global $app;
    $q = $outlet->from("Ukon");

    //mechanikovi zobrazovat pouze jeho ukony
    if($app->getUser()->pozice == ZAMESTNANEC::MECHANIK)
    {
      $q = $q->where("{Ukon.zamestnanec} = ?",array($app->getUser()->id));

      if ($this->stav == "vyrizene")
        $q = $q->where("{Ukon.stav} = ? AND {Ukon.zamestnanec} = ?", array(Ukon::VYRIZEN,$app->getUser()->id));
      elseif ($this->stav == "nevyrizene")
        $q = $q->where("{Ukon.stav} = ? AND {Ukon.zamestnanec} = ?", array(Ukon::NEVYRIZEN,$app->getUser()->id));   
    }
    
    //ostatnim zobrazovat vsechny ukony (hlavni mechanik, vedouci)
    else
    {
      if ($this->stav == "vyrizene")
        $q = $q->where("{Ukon.stav} = ?", array(Ukon::VYRIZEN));
      elseif ($this->stav == "nevyrizene")
        $q = $q->where("{Ukon.stav} = ?", array(Ukon::NEVYRIZEN));
    } 
    return $q;
  }
  
  public function dataCount()
  {
    return sizeof($this->getQuery()->find());
  }
  
  public function getTableData()
  {
    global $app;
    return $this->getQuery()
      ->limit($this->limit)
      ->offset($this->offset)
      ->find();
  }
  
  public function getStav()
  {
    return $this->stav;
  }
  
  /*
  * Nacte seznam mechaniku
  */
   public function seznamMechaniku()
   {
      global $outlet;
      return $outlet->from("Zamestnanec")->where("{Zamestnanec.pozice} = ? OR {Zamestnanec.pozice} = ?",array(ZAMESTNANEC::MECHANIK,ZAMESTNANEC::HLAVNI_MECHANIK))->find();
    
   }


   /*
   * - vkladani novych ukonu
   * - zmena ukonu
   */
   public function zmenaUkon($app)
    {
      global $outlet;
      $ukon = new Ukon;

      if(isset($_GET['id']))
        $ukon = $outlet->load("Ukon", $_GET['id']);
      
      $ukon->doba = $_POST['doba'];
      $ukon->typ = $_POST['typ'];
      $ukon->setStav($_POST['stav']);   

      if(isset($_POST['zamestnanec_id']))
      {
        $id = $_POST['zamestnanec_id'];
        
        if ($id) {
          $zamestnanec = $outlet->load("Zamestnanec", $id);
          $ukon->setZamestnanec($zamestnanec);
        }
        else
          $ukon->zamestnanec = NULL;
      }
      
     //pridani materialu
     $i = 0;
     while($_POST['material'.$i]!=NULL)
     {
        $ukon->setSpotreba($_POST['id_ukon'],$_POST['material'.$i],$_POST['mnozstvi'.$i],$app);
        $i++;
     }
     
     $outlet->save($ukon);
      return 0;
    }
}

?>
