<?php

class ZamestnanciController extends BaseController {
  private $stav;
  public function __construct()
  {
    parent::__construct();
    $this->stav = isset($_GET["stav"]) ? $_GET["stav"] : "vsichni";
  }
  
  
  public function show()
  {
    return array(
      
    );
  }

  /**
   *  - ziska udaje o zamestnanci z databaze podle id z GET
   *  - vyuziva se napr. ve formularich  
  */
  public function getZamestnanec()
  {
    global $outlet;
    
    if (isset($_GET["id"])) {
      $zam = $outlet->load("Zamestnanec", $_GET["id"]);
      return $zam;
    }
    
    return new Zamestnanec;
  }

  public function getTableColumns()
  {
    return array("Jméno", "Pozice", "Specializace", "Vytížení");
  }

  //specifikuje dat z databaze
  private function getQuery()
  {
    global $outlet;
    //vytvoreni pole trid zamestnancu
    $zamestnanci = $outlet->from("Zamestnanec")->find();

    //definice poli pro roztridovani volnych a obsazenych zamestnancu
    $zamestnanci_volni = array();
    $zamestnanci_plni = array();
 
    //prochazime jednotlive zamestnance
    foreach ($zamestnanci as $zamestnanec)
    {  
      //delime na volne a obsazene zamestnance      
      if ($zamestnanec->getVytizeni() == ZAMESTNANEC::VOLNY) 
      { 
        array_push($zamestnanci_volni, $zamestnanec); 
      }   
      else 
      {
         array_push($zamestnanci_plni, $zamestnanec); 
      }

    }

    //vracime podle zvolene zalozky      
    if ($this->stav == "volni")   
      return $zamestnanci_volni;
    elseif ($this->stav == "zamestnani")     
      return $zamestnanci_plni;   
    else    
      return $zamestnanci;
  } 

  public function dataCount()
  {
    return sizeof($this->getQuery());
  }
  
  public function getTableData()
  {
    global $outlet;
      return $this->getQuery();
  }

  //vraci stav zamestnany/volny/vsechny
  public function getStav()
  {
    return $this->stav;
  }

  //metoda pro upravy udaji zamestnance a vkladani novych zamestnancu
  public function zmenaZamestnanec($app)
  {
    global $outlet;
    $zamestnanec = new Zamestnanec;
    $id = isset($_GET['id']) ? $_GET['id'] : 0;
 
    if(isset($_GET['id']))
      $zamestnanec=$outlet->load("Zamestnanec",$_GET['id']);
    
    $zamestnanec->uziv_jmeno=$_POST['uziv_jmeno'];
    
    if (!empty($_POST['heslo1'])) {
      if($_POST['heslo1'] == $_POST['heslo2'])
        $zamestnanec->heslo=$app->passwd($_POST['heslo1']);
      else
        $app->addError("Chybne zadane kontrolní heslo");
    }

    $zamestnanec->jmeno=$_POST['jmeno'];
    $zamestnanec->prijmeni=$_POST['prijmeni'];
    $zamestnanec->telefon=$_POST['telefon'];
    $zamestnanec->pozice=$_POST['pozice'];
    $zamestnanec->specializace=$_POST['specializace'];
    $zamestnanec->mzda=$_POST['hodinova_mzda'];

    //kontrola proti duplicitnimu vkladani
    $dupl = $outlet->select("Zamestnanec","WHERE {Zamestnanec.uziv_jmeno} = ?", array($zamestnanec->uziv_jmeno));
   
    if ($dupl and ($id == 0 or $dupl[0]->id != $id)) {
      $app->addError("Uzivatelské jméno již existuje.");
      $app->setAction("edit");
    }
    else {
      try {
        $outlet->save($zamestnanec);
        $app->setAction("list");
      } catch (PDOException $e) {
        $app->addError("Byla zadána chybná nebo neúplná data.");
      }
    }
  }
}

?>
