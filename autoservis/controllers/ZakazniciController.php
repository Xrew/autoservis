<?php

class ZakazniciController extends BaseController {
  public function __construct()
  {
    parent::__construct();
  }
  
  public function show()
  {
    return array(
      
    );
  }
   
  /**
    * - vraci udaje o zakaznikovi dle id z GET
    * - vyuziva se napr pri vyplnovani formularu  
  */
  public function getZakaznik()
  {
    global $outlet;
    
    if (isset($_GET["id"])) {
      $zak = $outlet->load("Zakaznik", $_GET["id"]);
      return $zak;
    }
    return new Zakaznik;
  }

  public function getTableColumns()
  {
    return array("Jméno", "Adresa", "Telefon");
  }
  
  //citac radku na stranku -> strankovani
  public function dataCount()
  {
      global $outlet;
      return sizeof($outlet->from("Zakaznici"));
  }
  
  public function getTableData()
  {
    global $outlet;
    return $outlet->select("Zakaznik");
  }

  /**
  * Metoda pro upraveni 
  * nebo vytvoreni noveho zakaznika
  */
  public function zmenaZakaznik($app)
  {
    global $outlet;
    $zakaznik = new Zakaznik;
    
    if(isset($_GET['id']))
      $zakaznik = $outlet->load("Zakaznik", $_GET['id']);
 
    $zakaznik->jmeno = $_POST['jmeno'];
    $zakaznik->prijmeni = $_POST['prijmeni'];
    $zakaznik->ulice = $_POST['ulice'];
    $zakaznik->mesto = $_POST['mesto'];
    $zakaznik->psc = $_POST['psc'];
    $zakaznik->telefon = $_POST['telefon'];
    $zakaznik->email = $_POST['email'];

    $outlet->save($zakaznik);
    
    //zde bude mozno rozhodovat v pripade chyby co se stane (list/form)
    $app->setAction("list"); 
    return 0;
  }
}
?>
