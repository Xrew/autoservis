<?php

class ProsperitaController {
 
  /*
  * Zjisti celkove prijmy firmy v poslednim roce
  */
  public function getPrijmyToGraph()
  {   
    global $outlet;
    $objednavka = new Objednavka;
    $mesice = array("", "led", "uno", "bre", "dub", "kve", "cer",
                        "crv", "srp", "zar", "rij", "lis", "pro");
   
    //zjisteni rozmezi casu
    $dnesDatum = new DateTime();
    $lonskeDatum = new DateTime();
    $lonskeDatum->modify('-1 year');

    //nacteni objednavek v rozmezi zadanem $dnesDatum a $lonskeDatum
    $objednavky = $outlet->select("Objednavka","WHERE ({Objednavka.termin_vyrizeni} >= ? AND {Objednavka.termin_vyrizeni} <= ?)",array($lonskeDatum->format('Y-m-d'),$dnesDatum->format('Y-m-d'))); 
  
    //asociativni pole mesicu a prijmu k nim
    $prijmy = array();

    //zjistime prijmy v jednotlivych mesicich
    for($i = $lonskeDatum; $i <= $dnesDatum; $i->modify('+ 1 month'))
    {
      $time = $i->format("U");
      $mesic = $mesice[intval(strftime("%m", $time))] . strftime(" %y", $time);
      
      $prijmy["$mesic"] = 0;      
      foreach ($objednavky as $objednavka)
      {
        if($objednavka->termin_vyrizeni->format('Y-m') == $i->format('Y-m'))
        {
           $uctovano_zakaznikovi = $objednavka->getCena();
           $skutecny_prijem = $uctovano_zakaznikovi - $objednavka->getVyplataMechanikum() - $objednavka->getCenaMaterial();
           $prijmy["$mesic"] += $skutecny_prijem;                  
        }
      }    
    }
    return($prijmy);
  }
}
