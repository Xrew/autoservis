/**
 * Pocesteni validatoru formularu pro jQuery.
 */
$.extend($.validity.messages, {
    require:"Toto pole je povinné.",
    match:"Pole má neplatný formát.",
    integer:"Pole musí být celé kladné číslo.",
    date:"Pole musí mít formát data. (např. 04.05.2006)",
    email:"Pole musí být emailová adresa.",
    url:"#{field} musí mít formát URL adresy.",
    number:"#{field} musí být číslo.",
    zip:"Pole musí být pětimístné PSČ. (např. 12345)",
    phone:"#{field} musí být telefonní číslo.",
    time24:"#{field} musí mít formát času. (např. 23:00)",

    // Value range messages:
    lessThan:"#{field} musí být menší než #{max}.",
    lessThanOrEqualTo:"#{field} musí být menší nebo rovno #{max}.",
    greaterThan:"#{field} musí být větší než #{min}.",
    greaterThanOrEqualTo:"#{field} musí být větší nebo rovno #{min}.",
    range:"#{field} musí být mezi #{min} a #{max}.",

    // Value length messages:
    tooLong:"#{field} může mít maximálně #{max} znaků.",
    tooShort:"#{field} musí mít alespoň #{min} znaků.}",

    // Aggregate validator messages:
    equal:"Hodnoty se neshodují.",
    distinct:"Hodnoty se opakují.",
    sum:"Součet hodnot není #{sum}.",
    sumMax:"Součet musí být menší než #{max}.",
    sumMin:"Součet musí být větší než #{min}.",

    nonHtml:"#{field} nesmí obsahovat HTML kód.",

    generic:"Neplatný."
});

$.validity.setup({ defaultFieldName:"pole" });

$.extend($.validity.patterns, {
    date:/^(0[1-9]|[12]\d|30|31)\.([01]\d)\.\d{4}$/,
    zip: /^(\d{5}|\d{3} \d{2})$/,
    phone: /^[0-9+\- ]+$/
});


/* Czech initialisation for the jQuery UI date picker plugin. */
/* Written by Tomas Muller (tomas@tomas-muller.net). */
jQuery(function($){
	$.datepicker.regional['cs'] = {
		closeText: 'Zavřít',
		prevText: '&#x3c;Dříve',
		nextText: 'Později&#x3e;',
		currentText: 'Nyní',
		monthNames: ['leden','únor','březen','duben','květen','červen',
        'červenec','srpen','září','říjen','listopad','prosinec'],
		monthNamesShort: ['led','úno','bře','dub','kvě','čer',
		'čvc','srp','zář','říj','lis','pro'],
		dayNames: ['neděle', 'pondělí', 'úterý', 'středa', 'čtvrtek', 'pátek', 'sobota'],
		dayNamesShort: ['ne', 'po', 'út', 'st', 'čt', 'pá', 'so'],
		dayNamesMin: ['ne','po','út','st','čt','pá','so'],
		weekHeader: 'Týd',
		dateFormat: 'dd.mm.yy',
		firstDay: 1,
		isRTL: false,
		showMonthAfterYear: false,
		yearSuffix: ''};
	$.datepicker.setDefaults($.datepicker.regional['cs']);
});


/**
* Vybrat mechanika pomoci AJAXU a vlozit ho do formulare
*/
function selectMechanik(elm) {
  var inputs = ['#fm_jmeno', '#fm_prijmeni','#fm_uziv_jmeno'];
   
  // Najit mechanika
  id = elm.value;

    $.get("ajax.php?fn=getmechanik&id=" + id, function(data) {
      if (!data) return;
      var data = eval('(' + data + ')');
      
      for (var i = inputs.length - 1; i >= 0; --i)
        $(inputs[i]).val(data[i]).attr('disabled', 'disabled');
    });
  
}

/**
 * Vybrat zakaznika pomoci AJAXu a vlozit ho do formulare.
 */
function selectCustomer(elm) {
  var inputs = ['#fm_jmeno', '#fm_prijmeni',
    '#fm_ulice', '#fm_mesto', '#fm_psc', '#fm_telefon', '#fm_email'];
  
  // Novy zakaznik
  if (elm.value == '') {
    for (var i = inputs.length - 1; i >= 0; --i)
      $(inputs[i]).removeAttr('disabled').val('');
  }
  
  // Najit zakaznika
  else {
    var id = elm.value;

    $.get("ajax.php?fn=getcustomer&id=" + id, function(data) {
      if (!data) return;
      var data = eval('(' + data + ')');
      
      for (var i = inputs.length - 1; i >= 0; --i)
        $(inputs[i]).val(data[i]).attr('disabled', 'disabled');
    });
  }
}

/**
 * Kliknuti na tlacitko Pridat dalsi ukon.
 */
function addNextTask(elm)
{
  var lastRow = $(elm).parent().parent().prev();
  var newRow = lastRow.clone().fadeOut(0);
  newRow.find('input').val('');
  lastRow.after(newRow);
  newRow.fadeIn();
}

/**
 * Kliknuti na tlacitko Pridat dalsi ukon.
 */
function addNextMaterial(elm)
{
  var lastRow = $(elm).parent().parent().prev();
  var newRow = lastRow.clone().fadeOut(0);
  var lastID = parseInt(lastRow.find('select').attr('name').substring(8));
  newRow.find('select').attr('name', 'material' + (lastID + 1));
  newRow.find('input').attr('name', 'mnozstvi' + (lastID + 1));
  lastRow.after(newRow);
  newRow.fadeIn();
}

/**
 * Kliknutí na tlačítko Odstranit v tabulce.
 */
function confirmDelete(elm)
{
  var row = $(elm).parent().parent();
  row.addClass('active');
  var answer = confirm('Opravdu chcete odstranit zvolenou položku?');
  row.removeClass('active');
  return answer;
}

/**
 * Obnovit data ve formulari
 */
function refreshFormData(post)
{
  for (var k in post) {
    var elm = $('*[name=' + k + ']');
    if (!elm)
      continue;
    
    switch (elm.get(0).tagName.toLowerCase()) {
      case 'input':
        elm.val(post[k]);
      break;
      case 'textarea':
        elm.text(post[k]);
      break;
      case 'select':
        var opt = elm.find('option[value=' + post[k] + ']')
        if (opt)
          opt.attr('selected', true);
      break;
    }
  }
}

$(document).ready(function() {
  // Vyber datumu ve formulari
  $('.date').datepicker();
  
  // Validace formularu
  $('form').validity(function() {
		$('.required').require();
		$('.date').match('date');
		$('.zip').match('zip');
		$('.email').match('email');
		$('.integer').match('integer');
		$('input[type=password].equal').equal('Hesla se neshodují');
	});
  
  // Vlozit hvezdicku pred povinna pole
  $('input.required').each(function(i) {
    $(this).parent().prev().append($('<div class="star"></div>'));
  });
  
  $('table.list tr td').bind('click', function(){
    location.href = $(this.parentNode).find('a.show_btn').attr('href');
    //$(this.parentNode).find('.show_btn').click();
  });
});
