<?php
  header('Content-Type: text/html; charset=utf-8');
  require "config.php";
  
  $outlet = Outlet::getInstance();
  $outlet->createProxies();
  $outlet->query('SET NAMES UTF8');

  /**
  *  Vyplneni informaci o Zakaznikovi
  */
  function getCustomer()
  {
    global $outlet;
    if (!isset($_GET["id"]) or empty($_GET["id"]))
      return "";
      
    $id = intval($_GET["id"]);
    $zak = $outlet->load("Zakaznik", $id);
    if (!$zak) return "";
    
    $arr = array(
      $zak->jmeno, $zak->prijmeni,
      $zak->ulice, $zak->mesto, $zak->psc,
      $zak->telefon, $zak->email
    );
    
    return $arr;
  }


  /**
  *  Vyplneni informaci o Mechanikovi
  */
  function getMechanik()
  {
    global $outlet;
    if (!isset($_GET["id"]) or empty($_GET["id"]))
      return "";
      
    $id = intval($_GET["id"]);
    $mech = $outlet->load("Zamestnanec", $id);
    
    if (!$mech)
      return "";
    
    $arr = array(
      $mech->jmeno, $mech->prijmeni,
      $mech->uziv_jmeno
    );
    
    return $arr;
  }
  
  
  $data = "";
  $fn = isset($_GET["fn"]) ? $_GET["fn"] : "";
  
  // Zavolat obsluznou funkci
  switch ($fn) {
    case "getcustomer":
      $data = getCustomer();
    break;
    case "getmechanik":
      $data = getMechanik();      
    break;
  }
  
  // Vratit JSON data
  echo json_encode($data);
?>
