-- Skript SELECT pro databazi Autoopravny
-- Projekt do predmetu Databazove systemy
-- Autori: Vojtech Smejkal, xsmejk07@stud.fit.vutbr.cz
--         Tomas Sychra, xsychr03@stud.fit.vutbr.cz

-- odstraneni sekvenci a tabulek

-- DROP TABLE zakaznik CASCADE;
-- DROP TABLE objednavka CASCADE;
-- DROP TABLE stav_objednavky CASCADE;
-- DROP TABLE zamestnanec CASCADE;
-- DROP TABLE ukon CASCADE;
-- DROP TABLE material CASCADE;
-- DROP TABLE spotreba CASCADE;

SET NAMES 'utf8';
-- tabulka zakaznik
CREATE TABLE zakaznik (
	id_zakaznika INT  NOT NULL AUTO_INCREMENT,
	jmeno VARCHAR(30) NOT NULL,
	prijmeni VARCHAR(30) NOT NULL,
	ulice VARCHAR(40),
	mesto VARCHAR(40),
	PSC INT,
	telefon VARCHAR(20),
	email VARCHAR(50),
	PRIMARY KEY (id_zakaznika)
) ENGINE=InnoDB  
  DEFAULT CHARACTER SET utf8
  DEFAULT COLLATE utf8_general_ci;



-- tabulka stav objednavky
CREATE TABLE stav_objednavky (
	id_stavu INT  NOT NULL AUTO_INCREMENT,
	nazev VARCHAR(20),
	PRIMARY KEY( id_stavu)
) ENGINE=InnoDB 
  DEFAULT CHARACTER SET utf8
  DEFAULT COLLATE utf8_general_ci;



-- tabulka objednavka
CREATE TABLE objednavka (
	id_objednavky INT  NOT NULL AUTO_INCREMENT,
	popis VARCHAR(1000) NOT NULL,
	datum_podani DATE NOT NULL,
	stav INT,
	termin_vyrizeni DATE,
	rc_vozidla VARCHAR(15) NOT NULL,
	zakaznik INT NOT NULL,
	PRIMARY KEY (id_objednavky),
	INDEX stav_in (stav),
	CONSTRAINT stav_fk  FOREIGN KEY (stav) REFERENCES 
	stav_objednavky (id_stavu),
	INDEX zakaznik_in(zakaznik),
	CONSTRAINT zakaznik_fk FOREIGN KEY (zakaznik) REFERENCES
	zakaznik (id_zakaznika)
) ENGINE=InnoDB 
  DEFAULT CHARACTER SET utf8
  DEFAULT COLLATE utf8_general_ci;


-- tabulka zamestnanec
CREATE TABLE zamestnanci (
	id_zamestnanec INT  NOT NULL AUTO_INCREMENT,
	jmeno VARCHAR(30) NOT NULL,
	prijmeni VARCHAR(30) NOT NULL,
  uziv_jmeno VARCHAR(40) NOT NULL,
  heslo VARCHAR(80) NOT NULL,
	telefon VARCHAR(20),
	pozice INT NOT NULL,
	specializace VARCHAR(60),
	odpracovano_hodin INT DEFAULT 0,
	mzda FLOAT,
	PRIMARY KEY (id_zamestnanec)
) ENGINE=InnoDB
  DEFAULT CHARACTER SET utf8
  DEFAULT COLLATE utf8_general_ci;



-- tabulka ukon
CREATE TABLE ukon (
	id_ukonu INT NOT NULL AUTO_INCREMENT,
	doba SMALLINT,
	typ VARCHAR(50),
	objednavka INT NOT NULL,
	zamestnanec INT ,
  stav INT NOT NULL,
	PRIMARY KEY(id_ukonu),
	INDEX objednavka_in(objednavka),
	CONSTRAINT objednavka_fk FOREIGN KEY (objednavka) REFERENCES
	objednavka (id_objednavky) ON DELETE CASCADE,
	INDEX zamestnanec_in(zamestnanec),
	CONSTRAINT zamestnanec_fk FOREIGN KEY (zamestnanec) REFERENCES
	zamestnanci(id_zamestnanec)
) ENGINE=InnoDB 
  DEFAULT CHARACTER SET utf8
  DEFAULT COLLATE utf8_general_ci;


-- tabulka material
CREATE TABLE material (
	id_materialu INT NOT NULL  AUTO_INCREMENT,
  kod VARCHAR(20) NOT NULL,
	typ VARCHAR(50) NOT NULL,
	mnozstvi FLOAT NOT NULL,
	cena FLOAT NOT NULL,
	PRIMARY KEY (id_materialu)
) ENGINE=InnoDB
  DEFAULT CHARACTER SET utf8
  DEFAULT COLLATE utf8_general_ci;


-- tabulka spotreba
CREATE TABLE spotreba (
	ukon INT NOT NULL,
	material INT NOT NULL,
	mnozstvi FLOAT NOT NULL,
	INDEX ukon_in(ukon),
	INDEX material_in(material),
	CONSTRAINT ukon_fk FOREIGN KEY (ukon) REFERENCES
	ukon(id_ukonu),
	CONSTRAINT material_fk FOREIGN KEY (material) REFERENCES
	material(id_materialu)
) ENGINE=InnoDB 
  DEFAULT CHARACTER SET utf8
  DEFAULT COLLATE utf8_general_ci;





-- naplneni daty
-- tabulka zakaznik
INSERT INTO zakaznik ( jmeno , prijmeni , ulice , mesto , PSC , telefon , email ) VALUES
				  (  'Josef' , 'Novák' , 'Semilasso 1' , 'Brno' , 60200 , '602123456' , 'email@email.cz') ,
				  (  'Martin' , 'Novák' , 'Hradská 2' , 'Praha' , 14700 , '777123526' , 'email2@email.cz') ,
				  (  'Franta' , 'Lord' , 'Kotlařská 17' , 'Olomouc' , 14700 , '776123527' , 'email3@email.cz') ,
				  (  'Lojza' , 'Press'  ,'Masarykova 25' , 'Opava' , 25063 , '605123528' , 'email4@email.cz') ,
				  (  'Jakub' , 'Bonn' , 'Havířská 8' , 'Ostrava' , 14750 , '603123527' , 'email5@email.cz') ;

-- tabulka material
INSERT INTO material (  typ , kod , mnozstvi , cena ) VALUES
				 (  'kolo', 'K1' , 10 , 400) ,
				 (  'kryt světla', 'K2' , 50 , 1950) ,
				 (  'žárovka' ,'K3' ,50  , 50) ,
				 (  'šroub' , 'K4', 500 , 1) ,
				 (  'olej' , 'K5', 20 , 250) ,
				 (  'tlumiče' , 'K6', 10 , 8500) ;

-- tabulka stav_objednavky
INSERT INTO stav_objednavky ( nazev)  VALUES ( 'Prijato'), ( 'Vyrizeno');

-- tabulka zamestnanec
INSERT INTO zamestnanci ( jmeno , prijmeni , uziv_jmeno, heslo, telefon, pozice, specializace, odpracovano_hodin, mzda ) VALUES
				 (  'Roman' , 'Pavot' , 'xpavot00', '54d5cb2d332dbdb4850293caae4559ce88b65163f1ea5d4e4b3ac49d772ded14', '+420521365123' , 1 , 'programovani ridicich jednotek' , 200 , 50) ,
				 (  'Man' , 'Ronex' , 'xronex00', '54d5cb2d332dbdb4850293caae4559ce88b65163f1ea5d4e4b3ac49d772ded14', '+420589632145' , 2 , 'mechanicke zavady' , 80 , 80) ,
				 (  'Majitel' , 'Firmy' , 'majitel', '54d5cb2d332dbdb4850293caae4559ce88b65163f1ea5d4e4b3ac49d772ded14', '+420589632145' , 3 , '' , 0, 0) ;

-- tabulka  objednavka
INSERT INTO objednavka (  popis , datum_podani , stav , termin_vyrizeni , rc_vozidla , zakaznik ) VALUES
				( 'výměna kola , opravení světla' ,'2007-10-7', 2 , '2010-11-3', 'UOL-5892', 1 ) ,
				( 'seřízení brzd' , '2008-08-3', 2 , '2010-10-3', 'UPK-2517', 2 ) ,
				( 'výměna oleje' , '2009-10-3', 2 ,'2010-12-3', 'PNK-2015', 3 ) ,
				( 'výměna tlumičů' , '2010-01-3', 1 ,'2010-02-3', 'KPI-1571', 1 ) ,
				( 'oprava řídící jednotky , opravení světla' , '2010-05-3', 1 , '2010-12-3', 'UOL-5892', 5 ) ;


-- tabulka  ukon
INSERT INTO ukon ( doba , typ ,  objednavka , zamestnanec ,stav ) VALUES
				(1 , 'výměna kola' ,  1 , 2, 1),
				(2 , 'opravení světla' ,  1, 2 , 1),
				(2 , 'seřízení brzd' ,  2 , 2, 1),
				(1 , 'výměna oleje' ,  3 , 2, 1);


-- tabulka spotreba 
INSERT INTO spotreba (ukon , material , mnozstvi )VALUES
				( 1 , 1 , 1 ),
				( 1 , 4 , 4 ),
				( 2 , 2 , 1 ),
				( 2 , 3 , 1 ),
				( 4 , 5 , 1 );

COMMIT;


