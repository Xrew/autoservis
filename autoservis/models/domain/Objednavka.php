<?php
  class Objednavka {
    const PRIJATA = 1;
    const VYRIZENA = 2;
    
    public $id;
    public $popis;
    public $datum_podani;
    public $termin_vyrizeni;
    public $rc_vozidla;
    public $stav;
    
    public $zakaznik;
    
    public function __construct() {
      $nextWeek = time() + (7 * 24 * 60 * 60);
      $this->datum_podani = new DateTime;
      $this->termin_vyrizeni = new DateTime("@$nextWeek");
    }
    
    //zjistuje textovou podobu stavu objednavky z databaze (tabulka StavObjednavky)
    public function getStavObjednavky()
    {
      global $outlet;      
      $stav= $outlet->from("StavObjednavky")
                    ->where("{StavObjednavky.id} = ?",array($this->stav))
                    ->find();
      //vzdy pouze jediny stav
      return $stav[0];
    }

    //vraci zakaznika, ktery si objednavku zadal
    public function getZakaznik() {
      global $outlet;
      $zakaznik = $outlet->load("Zakaznik",$this->zakaznik);
      return $zakaznik;
    }

    //prirazuje objednavce zakaznika
    public function setZakaznik(Zakaznik $zakaznik) {
      $this->zakaznik = $zakaznik->id;
    }

    //outlet ORM - nevyuzivane
    public function getUkons() {}

    //vraci pole ukonu, ktere nalezi objednavce
    public function getUkony() {
      global $outlet;
      return $outlet->select("Ukon","WHERE {Ukon.objednavka}=?",array($this->id));
    }

    //vraci stav objednavky - nepotrebne, drive uzivane, nutno radne otestovat DB
   /* public function getStav() {
      global $outlet;      
      $stav= $outlet->from("StavObjednavky")
                    ->where("{StavObjednavky.id} = ?",array($this->stav))
                    ->find();
      //vzdy pouze jediny stav
      return $stav[0];   
    }*/
    
    //nastavuje stav objednavky
    public function setStav(StavObjednavky $stav) {
      $this->stav = $stav;
    }
    
    //vraci cislo objednavky upravene pro vypis do views
    public function cisloObjednavky() {
      $inkrement = 14356;
      return $this->id + $inkrement;
    }
    
    //vraci datum podani pripravene pro vypis 
    public function getDatum() {
      return $this->datum_podani->format("d.m.Y");
    }
    
    //vraci termin vyrizeni pripraveny pro vypis
    public function getTermin() {
      return $this->termin_vyrizeni->format("d.m.Y");
    }

    //spocita cenu materialu na objednavce
    public function getCenaMaterial()
    {
      $ukony=$this->getUkony();
      $material_cena = 0;

      //prochazime ukony k jednotlivym objednavkam
      foreach ($ukony as $ukon)
        $material_cena += $ukon->getCenaMaterial() ;

    return $material_cena;
    }

    /*
     * Navratova hodnota je kolik zamestnavatel
     * vyplati zamestnancum na teto objednavce
    */
    public function getVyplataMechanikum() {
      $ukony = $this->getUkony();     
      $vyplataMechanikum = 0;
      foreach ($ukony as $ukon)
        $vyplataMechanikum += $ukon->getVyplataMechanika() ;
     // echo $vyplataMechanikum;
      return $vyplataMechanikum;
    }

    /*
     * Spocita celkovou cenu objednavky, 
     * kterou uctujeme zakaznikovi , teday-> + zisk
    */
   public function getCena() {
     return ($this->getCenaMaterial()* 1.3 + $this->getVyplataMechanikum()* 1.5);  
   }
  
    //kontroluje zda-li objednavka obsahuje nejake ukony
    public function existUkon($typ)
    {
        global $outlet;
        $ukon = $outlet->select("Ukon","WHERE {Ukon.objednavka} = ? AND {Ukon.typ} = ? ", array($this->id,$typ));
        return $ukon!=NULL?1:0;
    }
  }
?>
