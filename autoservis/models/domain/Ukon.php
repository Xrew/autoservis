<?php
  class Ukon {
      const NEVYRIZEN = 0;
      const VYRIZEN = 1;

      public $id;
      public $narocnost;
      public $typ;
      public $doba;     
      public $stav;
      public $objednavka;
      public $zamestnanec;
      
      //vyzaduje ORM
      public function getUkons(){}

      //nastaveni a zjisteni ke ktere objednavce, ktery ukon patri
      public function getObjednavka() {
        global $outlet;
        $obj = $outlet->load("Objednavka",$this->objednavka);
        return $obj!=NULL?$obj:new Objednavka;
      }

      //priradi ukon k objednavce
      public function setObjednavkaToUkon(Objednavka $objednavka) {
        $this->objednavka=$objednavka->id;
      }

      //nastaveni a zjisteni kteremu mechanikovy ukon patri
      public function getZamestnanec() {
        global $outlet;
        
        if($this->zamestnanec!=NULL)
          $zam = $outlet->load("Zamestnanec",$this->zamestnanec);
        else
          $zam = NULL;        
        return $zam;
      }

      //prirazeni ukonu mechanikovi
      public function setZamestnanec(Zamestnanec $zamestnanec) {
        if($zamestnanec->id!=NULL)
          $this->zamestnanec=$zamestnanec->id;
        else
          $this->zamestnanec = NULL;
      }

      //zjisteni spotreby materialu k danemu ukonu
      public function getMaterial() {
        global $outlet;
        return $outlet->select("Spotreba","WHERE {Spotreba.idUkon}=?",array($this->id));
      
      }

      //nastavuje spotrebu materialu
      public function setSpotreba($ukon,$kod,$mnozstvi,$app) {
        global $outlet;
        
        $material = $outlet->select("Material","WHERE {Material.kod} = ?",array($kod));
        $mnozstviSklad = $material[0]->mnozstvi;
      
        if(!($mnozstviSklad-$mnozstvi<0))          
        {
          if($mnozstvi!=0)   
          {
            $spotreba = $outlet->select("Spotreba","WHERE {Spotreba.idUkon} = ? AND {Spotreba.idMaterial} = ?",array($ukon,$material[0]->id)); 
           
            if($spotreba==NULL) 
            {         
              $spo = new Spotreba;
            }
            else
              $spo = $spotreba[0];

            $spo->setTypMaterial($this->id,$material[0]->id,$mnozstvi);  
            $material[0]->mnozstvi-=$mnozstvi;
            $outlet->save($material[0]);     
            $outlet->save($spo);
         }
         
         $app->setAction("list");
        }

        else
        {
          $app->addError("Množství materiálu s kodem $kod nebylo možné k úkonu č.: $ukon->id přidat. Vámi zadané množství: $mnozstvi  je totiž větší nežli je dostupné množství: $mnozstviSklad na skladě.".
                         " Použili-li jste opravdu tento materiál, kontaktujte majitele firmy. K této nekonzistenci dojde v případě nedbalosti".
                          " některého ze zaměstnanců. Děkujeme"); 
          $app->setAction("edit");
        }
      }

      // Zjistuje cenu materialu ktery jsme potrebovali pro dany ukon
      public function getCenaMaterial()
      {
        $spotreba = $this->getMaterial();
        $cena = 0;
     
         foreach($spotreba as $mat)
        {
          $cena += $mat->mnozstvi * $mat->getMaterialInfo()->cena;
        }
    
        return $cena;
      }

      //zjistuje kolik zaplatime mechanikovi za dany ukon
      public function getVyplataMechanika() {
        $zamestnanec = $this->getZamestnanec();
        if ($zamestnanec)
          return $zamestnanec->mzda * $this->doba;
        else
          return 0;
      }

      //vraci cislo ukonu urcene pro vypis do views
      public function cisloUkonu() {
        $inkrement = 1234;
        return $this->id+ $inkrement;
      }

     /*
      * V databazi je ulozeno 1 / 0 
      * Funkce vrací ano/ne
     */
     public function getStav() {
        return $this->stav == 1 ? "Ano" : "Ne";    
     }

    /** 
    *  nastavuje stav ukonu
    * kontroluje zda-li byly vsechny ukonu splneny -> jestli ano -> objednavka vyrizena
    */
    public function setStav($stav)
    {
       global $outlet;
       $this->stav=$stav;
       $objednavka = $this->getObjednavka();
       $ukony = $objednavka->getUkony();
       $objednavka->stav = OBJEDNAVKA::VYRIZENA;
     
       foreach($ukony as $ukon)
       {
          if($ukon->stav==0)
          {
            $objednavka->stav = OBJEDNAVKA::PRIJATA;
            break;
          }
       }
       $outlet->save($objednavka);
    }

     //vraci cenu ukonu urcenou pro zakaznika ( tedy i s profitem pro nas)
     public function getCena() {
        $profit = 1.5;
        $profit_material = 1.3;
        return ($this->getCenaMaterial() * $profit_material + $this->getVyplataMechanika() * $profit);  
     }

     //inicializuje novy ukon
     public function newUkon($typ) {
        global $outlet;
        $this->typ=$typ;
        $this->cena=0;
        $this->stav=0;
        $this->doba=0;
     }
  }
?>
