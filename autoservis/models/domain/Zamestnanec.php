<?php
  class Zamestnanec {
    const MECHANIK = 1;
    const HLAVNI_MECHANIK = 2;
    const VEDOUCI = 3;
    
    const VYTIZEN = 4;
    const VOLNY = 5;

    public $id;
    public $jmeno;
    public $prijmeni;
    public $uziv_jmeno;
    public $heslo;
    public $telefon;
    public $pozice;
    public $specializace;
    public $odpracovano_hodin;
    public $mzda;
    public $vytizeni;
 
    // vraci cele jmeno zamestnance
    public function celeJmeno() {
      return $this->jmeno . "&nbsp;" . $this->prijmeni;
    }
    
    //vraci pozici zamestnance ve firme
    public function getPozice() {
      switch ($this->pozice) {
        case self::MECHANIK: return "Mechanik";
        case self::HLAVNI_MECHANIK: return "Hlavní mechanik";
        case self::VEDOUCI: return "Vedoucí";
      }
    }

    //vraci ukony prirazene konkretnimu zamestnanci
    public function getUkony() {
      global $outlet;
      return $outlet->from("Ukon")->where("{Ukon.zamestnanec} = ?",array($this->id))->find();
    }

    //zjistuje zda-li ma zamestnanec prirazeny nejake ukony ->kdyz ne, zobrazi se to u nej
    public function getVytizeni() {
      global $outlet;
      $ukony = $outlet->select("Ukon","WHERE {Ukon.zamestnanec} = ? AND {Ukon.stav} = ?",array($this->id,UKON::NEVYRIZEN));
      if($ukony!=NULL)
        return ZAMESTNANEC::VYTIZEN;
      return ZAMESTNANEC::VOLNY;
    }

    //vyzaduje ORM
    public function getUkons() {}
    
  }



?>
