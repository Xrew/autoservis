<?php
  class Spotreba {
      public $idUkon;
      public $idMaterial;
      public $mnozstvi;
      
      //vraci pouzity typ materialu
      public function getMaterialInfo() {
        global $outlet;        
        $material= $outlet->select("Material","WHERE {Material.id} = ?",array($this->idMaterial));
        return $material[0];
        
      }

      //nastavi spotrebu a typ materialu  k urcitemu ukonu
      public function setTypMaterial($ukon,$id,$mnozstvi)
      {
        global $outlet;
        $this->idUkon = $ukon;
        $this->idMaterial = $id;
        $this->mnozstvi += $mnozstvi;        
      }

      //vyzaduje ORM 
      public function getMaterial() {}
      public function getUkon() {}
      public function setMaterial() {}
      public function setUkon() {}
  }



?>
