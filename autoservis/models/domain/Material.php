<?php
  class Material {
    const NEDOSTUPNY=0;
    public $id;
    public $kod;
    public $typ;
    public $mnozstvi;
    public $cena;
    
    // vraci cenu materialu pro vypis do tabulky
    public function getCena() {
      return $this->cena . "&nbsp;Kč";
    }
    public function getMaterials() {}

    //nacte data z databaze
    public function getTableData() 
    {
      global $outlet;
      return $outlet->select("Material");
    }
    
  }
?>
