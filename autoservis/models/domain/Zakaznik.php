<?php
  class Zakaznik {
    public $id;
    public $jmeno;
    public $prijmeni;
    public $ulice;
    public $mesto;
    public $psc;
    public $telefon;
    public $email;
      
    //vraci cele jmeno zakaznika
    public function celeJmeno() {
      return $this->jmeno . "&nbsp;" . $this->prijmeni;
    }
    
    //vraci celou adresu urcenou pro vypis do views
    public function adresa() {
      return $this->ulice . ",&nbsp;" . $this->mesto;
    }

    //vyzaduje ORM    
    public function getObjednavkas() {}
  }
?>
