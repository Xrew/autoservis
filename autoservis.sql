-- phpMyAdmin SQL Dump
-- version 3.3.2deb1
-- http://www.phpmyadmin.net
--
-- Počítač: localhost
-- Vygenerováno: Úterý 23. listopadu 2010, 21:27
-- Verze MySQL: 5.1.41
-- Verze PHP: 5.3.2-1ubuntu4.5

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Databáze: `autoservis`
--

-- --------------------------------------------------------

--
-- Struktura tabulky `material`
--

CREATE TABLE IF NOT EXISTS `material` (
  `id_materialu` int(11) NOT NULL AUTO_INCREMENT,
  `kod` varchar(20) NOT NULL,
  `typ` varchar(50) NOT NULL,
  `mnozstvi` float NOT NULL,
  `cena` float NOT NULL,
  PRIMARY KEY (`id_materialu`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=22 ;

--
-- Vypisuji data pro tabulku `material`
--

INSERT INTO `material` (`id_materialu`, `kod`, `typ`, `mnozstvi`, `cena`) VALUES
(1, 'K1', 'kolo', 10, 400),
(2, 'K2', 'kryt světla', 49, 1950),
(3, 'K3', 'žárovka', 50, 50),
(4, 'K4', 'šroub', 500, 1),
(5, 'K5', 'olej', 20, 250),
(6, 'K6', 'tlumiče', 10, 8500),
(10, 'sroub1', 'šroub křížový', 200, 15),
(11, 'sroub2', 'šroub šestihranný', 200, 30),
(12, 'sroub3', 'šroub půlený', 115, 10),
(13, 'cep1', 'čep vnitřní', 50, 50),
(14, 'cep2', 'čep vnější', 100, 80),
(15, 'cep3', 'čep univerzální', 15, 90),
(16, 'meric1', 'měřič rychlosti', 40, 500),
(17, 'meric2', 'měřič paliva', 30, 250),
(18, 'meric3', 'měřič teploty', 60, 120),
(19, 'meric4', 'měřič tlakování', 2, 350),
(20, 'meric5', 'měřič exhalací', 30, 800),
(21, 'sklo', 'přední sklo', 5, 8000);

-- --------------------------------------------------------

--
-- Struktura tabulky `objednavka`
--

CREATE TABLE IF NOT EXISTS `objednavka` (
  `id_objednavky` int(11) NOT NULL AUTO_INCREMENT,
  `popis` varchar(1000) NOT NULL,
  `datum_podani` date NOT NULL,
  `stav` int(11) DEFAULT NULL,
  `termin_vyrizeni` date DEFAULT NULL,
  `rc_vozidla` varchar(15) NOT NULL,
  `zakaznik` int(11) NOT NULL,
  PRIMARY KEY (`id_objednavky`),
  KEY `stav_in` (`stav`),
  KEY `zakaznik_in` (`zakaznik`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Vypisuji data pro tabulku `objednavka`
--

INSERT INTO `objednavka` (`id_objednavky`, `popis`, `datum_podani`, `stav`, `termin_vyrizeni`, `rc_vozidla`, `zakaznik`) VALUES
(1, 'výměna kola , opravení světla', '2007-10-07', 1, '2010-11-03', 'UOL-5892', 1),
(2, 'seřízení brzd', '2008-08-03', 2, '2010-10-03', 'UPK-2517', 2),
(3, 'výměna oleje', '2009-10-03', 2, '2010-12-03', 'PNK-2015', 3),
(4, ' výměna tlumičů', '2010-01-03', 1, '2010-02-03', 'KPI-1571', 1),
(5, 'oprava řídící jednotky , opravení světla', '2010-05-03', 1, '2010-12-03', 'UOL-5892', 5);

-- --------------------------------------------------------

--
-- Struktura tabulky `spotreba`
--

CREATE TABLE IF NOT EXISTS `spotreba` (
  `ukon` int(11) NOT NULL,
  `material` int(11) NOT NULL,
  `mnozstvi` float NOT NULL,
  KEY `ukon_in` (`ukon`),
  KEY `material_in` (`material`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Vypisuji data pro tabulku `spotreba`
--

INSERT INTO `spotreba` (`ukon`, `material`, `mnozstvi`) VALUES
(1, 1, 1),
(1, 4, 4),
(2, 2, 1),
(2, 3, 1),
(4, 5, 1),
(1, 2, 1),
(5, 21, 1);

-- --------------------------------------------------------

--
-- Struktura tabulky `stav_objednavky`
--

CREATE TABLE IF NOT EXISTS `stav_objednavky` (
  `id_stavu` int(11) NOT NULL AUTO_INCREMENT,
  `nazev` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id_stavu`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Vypisuji data pro tabulku `stav_objednavky`
--

INSERT INTO `stav_objednavky` (`id_stavu`, `nazev`) VALUES
(1, 'Přijato'),
(2, 'Vyřízeno');

-- --------------------------------------------------------

--
-- Struktura tabulky `ukon`
--

CREATE TABLE IF NOT EXISTS `ukon` (
  `id_ukonu` int(11) NOT NULL AUTO_INCREMENT,
  `doba` smallint(6) DEFAULT NULL,
  `typ` varchar(50) DEFAULT NULL,
  `objednavka` int(11) NOT NULL,
  `zamestnanec` int(11) DEFAULT NULL,
  `stav` int(11) NOT NULL,
  PRIMARY KEY (`id_ukonu`),
  KEY `objednavka_in` (`objednavka`),
  KEY `zamestnanec_in` (`zamestnanec`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Vypisuji data pro tabulku `ukon`
--

INSERT INTO `ukon` (`id_ukonu`, `doba`, `typ`, `objednavka`, `zamestnanec`, `stav`) VALUES
(1, 1, 'výměna kola', 1, NULL, 0),
(2, 2, 'opravení světla', 1, 2, 1),
(3, 2, 'seřízení brzd', 2, 2, 1),
(4, 1, 'výměna oleje', 3, 2, 1),
(5, 50, 'výměna předního skla', 4, 2, 1),
(6, 0, 'kontrola geometrie', 4, NULL, 0);

-- --------------------------------------------------------

--
-- Struktura tabulky `zakaznik`
--

CREATE TABLE IF NOT EXISTS `zakaznik` (
  `id_zakaznika` int(11) NOT NULL AUTO_INCREMENT,
  `jmeno` varchar(30) NOT NULL,
  `prijmeni` varchar(30) NOT NULL,
  `ulice` varchar(40) DEFAULT NULL,
  `mesto` varchar(40) DEFAULT NULL,
  `PSC` int(11) DEFAULT NULL,
  `telefon` varchar(20) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id_zakaznika`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Vypisuji data pro tabulku `zakaznik`
--

INSERT INTO `zakaznik` (`id_zakaznika`, `jmeno`, `prijmeni`, `ulice`, `mesto`, `PSC`, `telefon`, `email`) VALUES
(1, 'Josef', 'Novák', 'Semilasso 1', 'Brno', 60200, '602123456', 'email@email.cz'),
(2, 'Martin', 'Novák', 'Hradská 2', 'Praha', 14700, '777123526', 'email2@email.cz'),
(3, 'Franta', 'Lord', 'Kotlařská 17', 'Olomouc', 14700, '776123527', 'email3@email.cz'),
(4, 'Lojza', 'Press', 'Masarykova 25', 'Opava', 25063, '605123528', 'email4@email.cz'),
(5, 'Jakub', 'Bonn', 'Havířská 8', 'Ostrava', 14750, '603123527', 'email5@email.cz');

-- --------------------------------------------------------

--
-- Struktura tabulky `zamestnanci`
--

CREATE TABLE IF NOT EXISTS `zamestnanci` (
  `id_zamestnanec` int(11) NOT NULL AUTO_INCREMENT,
  `jmeno` varchar(30) NOT NULL,
  `prijmeni` varchar(30) NOT NULL,
  `uziv_jmeno` varchar(40) NOT NULL,
  `heslo` varchar(80) NOT NULL,
  `telefon` varchar(20) DEFAULT NULL,
  `pozice` int(11) NOT NULL,
  `specializace` varchar(60) DEFAULT NULL,
  `odpracovano_hodin` int(11) DEFAULT '0',
  `mzda` float DEFAULT NULL,
  PRIMARY KEY (`id_zamestnanec`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=14 ;

--
-- Vypisuji data pro tabulku `zamestnanci`
-- Hesla nastavena na "bztchv2"
--

INSERT INTO `zamestnanci` (`id_zamestnanec`, `jmeno`, `prijmeni`, `uziv_jmeno`, `heslo`, `telefon`, `pozice`, `specializace`, `odpracovano_hodin`, `mzda`) VALUES
(1, 'Roman', 'Pavot', 'delnik', '87a0c4dde5872c316e44af0f8b8fe2f0f7d733dd7a36fbcff3ace23a14dd4c1a', '+420521365123', 1, 'programovani ridicich jednotek', 200, 50),
(2, 'Man', 'Ronex', 'mechanik', '87a0c4dde5872c316e44af0f8b8fe2f0f7d733dd7a36fbcff3ace23a14dd4c1a', '+420589632145', 2, 'mechanicke zavady', 80, 80),
(3, 'Pan', 'Majitel', 'majitel', '87a0c4dde5872c316e44af0f8b8fe2f0f7d733dd7a36fbcff3ace23a14dd4c1a', '+420589632145', 3, '', 0, 0);

--
-- Omezení pro exportované tabulky
--

--
-- Omezení pro tabulku `objednavka`
--
ALTER TABLE `objednavka`
  ADD CONSTRAINT `stav_fk` FOREIGN KEY (`stav`) REFERENCES `stav_objednavky` (`id_stavu`),
  ADD CONSTRAINT `zakaznik_fk` FOREIGN KEY (`zakaznik`) REFERENCES `zakaznik` (`id_zakaznika`);

--
-- Omezení pro tabulku `spotreba`
--
ALTER TABLE `spotreba`
  ADD CONSTRAINT `ukon_fk` FOREIGN KEY (`ukon`) REFERENCES `ukon` (`id_ukonu`),
  ADD CONSTRAINT `material_fk` FOREIGN KEY (`material`) REFERENCES `material` (`id_materialu`);

--
-- Omezení pro tabulku `ukon`
--
ALTER TABLE `ukon`
  ADD CONSTRAINT `objednavka_fk` FOREIGN KEY (`objednavka`) REFERENCES `objednavka` (`id_objednavky`) ON DELETE CASCADE,
  ADD CONSTRAINT `zamestnanec_fk` FOREIGN KEY (`zamestnanec`) REFERENCES `zamestnanci` (`id_zamestnanec`);
